# F23_Notes_2420

Course notes for BCIT 2420 Fall 2023
Intro to Linux and System Administration

## Summary

[week 1 notes](/notes/Week-1.md)
- intro
- setup DO

[week 2 notes](/notes/Week-2.md)
- vim
- digital ocean

[week 3 notes](/notes/Week-3.md)
- find
- grep
- file system

[week 4 notes](/notes/Week-4.md)
- creating users
- permissions
- ownership

[week 5 notes](/notes/Week-5.md)
- processes
- ps
- pkill

[week 6 notes](/notes/Week-6.md)
- bash
	- intro shell scripting
	- shebang, variables

[week 7 notes](/notes/Week-7.md)
- bash
	- conditionals
	- loops
	- command substitution

[week 8 notes](/notes/Week-8.md)
- bash
	- functions
	- case
	- getopts
	- exit status

[week 9 notes](/notes/Week-9.md)
- package managers
	- use apt
	- create a deb package
## assignments

[assignment 1](/assignments/as1.md)

[assignment 2](/assignments/as2.md)