---
tags:
  - BCIT
  - Linux
topics:
  - processes
  - ps
  - pkill
---

# Week 5 2420 

# Learning outcomes and topics

This week:
- add an alias to your .bashrc
- add a path to your system using a shell configuration file
- find the PID of a running process using `ps`
- Kill a running process with `pkill`

Next week:
- shell redirects
- intro to shell scripting

# assignment 1

If you still don't have a topic to work on please talk to me in class today so that we can find you a topic to work on.

# Shell configuration files

When the shell starts it reads at least one of several shell configuration files. These are sometimes referred to as start up files, because they are read when the program, in our case bash, starts.

## `.profile` or `.bash_profile`

A login shell will first load system wide defaults in `/etc/profile`. Afterwards it will read per-user specified configuration, stored in `~/.profile` or `~/.bash_profile`.

When you log out, commands in `~/.bash_logout` are executed.

## `.bashrc`

an interactive shell, like the one you work with when you open a terminal, sources the `~/.bashrc` file when it starts up.

## Adding a path to your system in `~/.profile`

Because the `~/.profile` file is sourced only on login, or only once it is a good place to put certain types of configuration. Like adding a  directory to your path.

To see your current path run the command below. This will print a colon separated list of all of the directories in your path.

```bash
echo $PATH
```

open up your `~/.profile` file in vim and add the following.

```bash
# add bin to path
if [ -d $HOME/bin ]; then
	PATH="$HOME/bin:$PATH"
fi
```

source the `~/.profile` file to read the file and apply any changes to your shell.

```bash
source ~/.profile
```

- [?] What would happen if we added this to `~/.bashrc`?
- [?] What is your path?

## Adding an alias to your `~/.bashrc` file

An alias is a little like a short cut. It is a way to run more complex commands with less typing.

Let's add an alias to change the default behavior of the `ls` command.

Let's add the following:
- group directories first
- color output
- show hidden files
- classify files, append a file indicator to some file types
- print human-readable file sizes

- [?] How can we find out how to do all of this?

**Reference:**

[Bash Startup Files](https://www.gnu.org/software/bash/manual/bash.html#Bash-Startup-Files)

[Interactive Shells](https://www.gnu.org/software/bash/manual/bash.html#Interactive-Shells)

# Executing commands

At this point in the course you have probably typed and run well over 100 commands already. But we haven't talked about what actually happens when you type a command and hit enter.

When you enter a command the first step is tokenization, the shell splits up the command into tokens, individual words, or characters separated by white space.

The next step is that the shell performs "Simple command expansion". During this process other types of expansion are performed[^1]. If after expansion a command is found, the shell searches for that command.

- First bash searches for functions
- then shell builtins (like cd)
- then finally commands in $PATH

If a command is found, or the command has a slash in it the shell attempts to execute the command in a separate execution environment.

Try this, open up your .bashrc file in vim and add the following bash function to the bottom of the file, save .bashrc and source it. Now try cding into a directory.

```bash
cd () {
  echo "cd doesn't exist anymore"
}  
```

- [!] Don't forget to remove this function and source your .bashrc file again.

**Reference:**

[Executing commands](https://www.gnu.org/software/bash/manual/bash.html#Executing-Commands)

[^1]: See [Bash Reference Manual Section 3.5 Shell Expansions](https://www.gnu.org/software/bash/manual/bash.html#Shell-Expansions) for more
# Processes

> A _process_ is the execution of a command by the Linux kernel. The shell that starts when you log in is a process, like any other. When you specify the name of a utility as a command, you initiate a process. When you run a shell script, another shell process is started, and additional processes are created for each command in the script. Depending on how you invoke the shell script, the script is run either by the current shell or, more typically, by a subshell (child) of the current shell. Running a shell builtin, such as cd, does not start a new process.
> - [A Practical Guide to Linux Commands, Editors, and Shell Programming, Fourth Edition](https://learning.oreilly.com/library/view/a-practical-guide/9780134774626/)

Just like directories processes have a hierarchical structure. They have parents and children.

Try running the following two commands

```bash
bash
ps --forest
```

- [?] what do you think is happening?

## PID

Every process is assigned a unique PID, Process Identification. Init is always PID 1, which is often referred to as "the mother of all processes"

## The `ps` utility

From `man ps` "ps - report a snapshot of the current processes."

The `ps` utility allows you to view information about a selection of the currently running processes.

To see all process you can use the -e option, think -e for everything

```bash
ps -e
```

## killing a running process

Sometimes you need to stop a running process. There are a few ways of doing this. The one we are going to look at today is the `pkill` command.

The pkill command will kill a process by name. 

Running the command below will kill the sshd server, which you need running so that you can connect to your server via ssh. So you probably don't want to run this command.

```bash
pkill sshd
```

pkill works like grep in that it matches a pattern, not the exact name.
### signals

- 15 (TERM) gracefully stop a process
- 9 (KILL) kill a process

By default `pkill` sends signal 15 so it will attempt to gracefully stop a process. 

If you wanted to use a different signal, you could use:

```bash
pkill -9 sshd
```

This will send the KILL signal.

You can also kill all processes started by a specific user(s).

Using the -u option you can supply a list of user names.

```bash
pkill -u mo,kim
```

## Exercise

Let's try some of this out.

To start with we want a process that we can kill without worry. For that we are going to create a simple infinite loop.

save the code snippet below into a file named 'sillyloop'

```bash
#!/bin/bash
while true; do
	x=1
done
```

Make the file executable

- [?] How do you make a file executable?

Now run the script as a background process, you can do this with the ampersand.

```bash
./sillyloop &
```

Now find your process with ps and grep

- [?] How can you do that?

Now kill the process.

- [?] How do you think you could do that?
- [?] How could you confirm the process is no longer running?

# even more cool process tools

Hopefully it doesn't surprise you that there are a lot of tools that can be used to learn about running processes. One of those is [`btop`](https://github.com/aristocratos/btop?ref=itsfoss.com) . You can install `btop` on Debian like this:

```bash
sudo apt update
sudo apt install btop
```

We will talk about `apt` in another class. After installing you can run it with the `btop` command.

# Week 5 practice

==Reminder: You should be doing everything as a regular user now, not as root.==

Write a `grep` command to find all of the files with the word "alias" in your home directory

Add aliases to `cp` `mv` and `rm` to make them a little safer by prompting the user before overwriting or deleting files. 

Use ps and grep to find the process ids of the ssh server (this will be more than one process and will be sshd (d for daemon)).

Which option would you use to only display process started by a specific user? Search the man page to find this.

Try to find the parent process id of a process. The process id of the process that started your searched process. For example if you search for bash, what is the process id of the process that started bash. Hint, look at the -o format option.

Try installing and playing with btop.

# Reading

From Linux: Bash Shell and Scripts, [Using file descriptors, file redirection, pipes and here documents](https://www.linkedin.com/learning/linux-bash-shell-and-scripts/using-file-descriptors-file-redirection-pipes-and-here-documents)

From Learning Linux Shell Scripting, [The Shebang](https://www.linkedin.com/learning/learning-linux-shell-scripting-2018/the-shebang)

From Learning Bash Scripting, [Displaying text with "echo"](https://www.linkedin.com/learning/learning-bash-scripting-17063287/displaying-text-with-echo)

From Learning Bash Scripting, [Working with variables](https://www.linkedin.com/learning/learning-bash-scripting-17063287/working-with-variables)

From Learning Bash Scripting, [Working with arguments](https://www.linkedin.com/learning/learning-bash-scripting-17063287/working-with-arguments)

# Todo

- [ ] Complete readings in section above
- [ ] Complete practice exercises
- [ ] Work on assignment 1