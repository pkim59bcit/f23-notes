---
tags:
  - BCIT
  - Linux
topics:
  - users
  - groups
  - permissions
---
# Week 4 2420 

## Learning outcomes and topics

This week:
- examine the relationship between users, groups and permissions in Linux.
- Discuss how these are used to aide security
- Create a new user with the `useradd` utility
- Create a password for your user with the `passwd` utility
- Add your new user to the sudo or wheel group to talk about granting the user sudo privileges.
- Use sudo as a regular user to temporarily elevate user privileges
- Look at file permissions with `ls`
- change file permissions with `chmod`
- change file ownership with `chown`

Next week:
- Processes

## Last week's practice Quiz review

- The shell we are using is bash
- To search for a man page use -k
- To write the contents of a buffer in vim to a file use `:w`
- The find utility can be used to execute a command on its results
- ==The utility used to view man pages is the pager `less`==
	- See week 2 notes
- configuration files are generally found in /etc
- cd is a shell builtin
- The /bin directory is not a pseudo filesystem
- `mkdir -p dir1/dir2` can be used to create nested directories dir2 inside of dir1
- all of the grep commands match the pattern
	- `[123]` will match the numbers 1 2 or 3
	- `[[:digit:]]` will match any digit
	- `.` will match any single character

Remember if you got 6/10 or less, it likely means that there is more you should be doing to keep up with class, or that you might need a little extra help to understand some of the material.
Are you doing the readings, taking notes and reviewing your notes? If you are uncertain of a topic in class and you don't want to see me during office hours you can send me a message on discord, or look into tutoring.

7/10 or better, keep doing what you are doing.

Always keep in mind with quizzes that they are meant to be a check in/evaluation. Redoing the quiz until to remember these 10 questions won't help you nearly as much as reviewing some of the course material.
## Users and groups and permissions

You have seen the output of the `ls -l` command a few times at this point. The output of this command nicely illustrates the relationship between users, groups and permissions. Every file (remember that everything is a file) belongs to a user and a group (just one of each). Files also have permissions for:
- The User that owns the file
- The Group that owns the file
- Other (everyone else)
Those permissions are *read*, *write* and *execute*. 
With these three things (users, groups and permissions) we can determine who can do what.

```
$ ls -al
total 0
-rw-r--r--  1  mh9  devs  9  Apr 12 11:42  test
^           ^  ^    ^     ^  ^             ^
|           |  |    |     |  |             └──  filename
|           |  |    |     |  └──  last modified date
|           |  |    |     └──  filesize in bytes
|           |  |    └── group the file belongs to
|           |  └──  user the file belongs to
|           └──  number of hard links
└──  file mode (- indicates a regular file) and permissions u/g/o
```

## Users

Everything in a Linux OS is owned by a user. We can group users into two categories:
- people, a regular user or root
- system users (files owned by a daemon)

users have a UID (user id) and belong to at least one group. When you create a new user, you also create a new group, which has a GID (group id). Generally these numbers are the same, but they don't have to be.

- UID 0 Is `root`
- UID 1 to 999 Are reserved for system users
- UID 65534 Is user `nobody`—
    - used, for example, for mapping remote users to some well-known ID, as is the case with [“Network File System”](https://learning.oreilly.com/library/view/learning-modern-linux/9781098108939/ch07.html#nfs)
- UID 1000 to 65533 and 65535 to 4294967294 Are regular users.

**Reference:**

[nobody user](https://wiki.ubuntu.com/nobody)
### Where is user information stored

#### /etc/passwd

The `passwd` file in `/etc` stores the following information
- username
- password, x indicates that an encrypted password is stored in the /etc/shadow file
- UID
- GID
- Name or GECOS This field contains the following information, separated by commas:
	- User’s full name or the application name.
	- Room number.
	- Work phone number.
	- Home phone number.
	- Other contact information.
- Location of home directory, this is generally `/home/<user-name>`
- location of login shell

```
mark:x:1001:1001:mark,,,:/home/mark:/bin/bash
[--] - [--] [--] [-----] [--------] [--------]
|    |   |    |     |         |        |
|    |   |    |     |         |        +-> 7. Login shell
|    |   |    |     |         +----------> 6. Home directory
|    |   |    |     +--------------------> 5. GECOS Real Name of user
|    |   |    +--------------------------> 4. GID
|    |   +-------------------------------> 3. UID
|    +-----------------------------------> 2. Password
+----------------------------------------> 1. Username
```

It is possible to read the `/etc/passwd` file as a regular user.

```bash
cat /etc/passwd
```

Because of this it isn't good practice to store a password directly in this file in plain text. So the password is stored in `/etc/shadow`

#### /etc/shadow

The shadow file stores users encrypted passwords as well as information about the users password. 

•   Login name

•   Encrypted password

•   Days since January 1, 1970, that password was last changed

•   Days before password may be changed

•   Days after which password must be changed

•   Days before password is to expire that user is warned

•   Days after password expires that account is disabled

•   Days since January 1, 1970, that account is disabled

•   A reserved field

```
mark:$6$.n.:17736:0:99999:7:::
[--] [----] [---] - [---] ----
|      |      |   |   |   |||+-----------> 9. Unused
|      |      |   |   |   ||+------------> 8. Expiration date
|      |      |   |   |   |+-------------> 7. Inactivity period
|      |      |   |   |   +--------------> 6. Warning period
|      |      |   |   +------------------> 5. Maximum password age
|      |      |   +----------------------> 4. Minimum password age
|      |      +--------------------------> 3. Last password change
|      +---------------------------------> 2. Encrypted Password
+----------------------------------------> 1. Username
```

**Reference:**

[Unix Epoch](https://learning.oreilly.com/library/view/linux-administration-a/9781260441710/ch6.xhtml#:-:text=UNIX%20Epoch%3A%20January%201%2C%201970)
### Creating a new "regular" user

- [?] Why do we need a new regular user?

- [?] Why can't we just keep using the root user?

![[root.png]]

### Creating a new user with `useradd`

`useradd` vs `adduser`

The `useradd` command is a low level command that can be used to create a new user. This command is available on almost every Linux OS.

The `adduser` command isn't available on every Linux OS.
- On some (Most Ubuntu based distros) it is a perl script
- On some it is an alias to useradd, so you are using `useradd` no matter which command you run.
- On many, it just isn't there and you will get a command not found error.

So, we are going to use `useradd` for all of our new users in 2420. It is difficult to not mix the two up, so you may want to tattoo this on your body somewhere highly visible.

#### `useradd`

The general syntax of the useradd command is:

```
useradd [options] <user-name>
```

To learn more about the utilities options see `man useradd`

**Example command**

Assume I want to make a new user, "mo" the basic command would be

```bash
useradd mo
```

depending on how the useradd utility is configured, and the results that you are hoping for, this might be all you need.

Unfortunately to get the results that we want we are going to need a few options.

```bash
useradd -ms /bin/bash <user-name>
```

- [?] What do the options in the command above do?
#### `useradd` configuration

Default configuration for the useradd utility can be found in `/etc/default/useradd`.
In our [Debian](https://www.debian.org/) servers the only configuration is setting the default shell to `/bin/sh` which in Debian is the dash shell.

### Give the user a password with `passwd`

Our new user should also have a password.

```bash
passwd <user-name>
```

This will prompt you to enter a password twice. For security reasons nothing will be displayed on the screen.

## Groups

A group in a Linux OS is a collection of users. Groups make it easier to assign permissions to multiple users.

You can view the members of a group with the `groups` command

```bash
groups <user-name>
```

You can create a new group with the `groupadd` command.

```
groupadd [OPTIONS] <group-name>
```

Similar to `/etc/passwd` the `/etc/group` file contains information about the groups that exist on your system.

### the `sudo` command

The sudo command allows a user to temporarily perform tasks with elevated privileges. Most Linux systems include either a `sudo` command or a `doas` command, which also allows a user to temporarily run commands with elevated privileges.

Debian includes the sudo command. `man sudo` to learn more.
### The sudo and or wheel group

Like other utilities in most Linux OSs, `sudo` has some configuration in the `/etc` directory.

| File | Purpose |
| --- | --- |
| /etc/sudo.conf | Configuration for the sudo command |
| /etc/sudoers | Configuration for who can use sudo |

The sudoers file will generally include a line that grants members of either the "sudo" group or the "wheel" group sudo privileges. 

For example, the sudoers file on my system contains the line `%wheel ALL+(ALL) ALL` Which grants members of the wheel group the ability to do everything with sudo.

- [?] Why do we have the sudo command? Why not just use root for everything?

### add a user to the sudo group to allow them to use sudo in Debian

```bash
usermod -aG sudo <user-name>
```

- [?] What do the a and G options do in the command above? They are important.

**Reference:**

[Sudo](https://wiki.archlinux.org/title/Sudo)

## Permissions

A Linux OS uses permissions on an individual resource (file) to determine who can do what with a file.

- r = read
- w = write
- x = execute
- `-` = a place holder for permissions the user does not have

These permissions are granted on a file to either the:
- user
- group
- other (everyone else)  


- [?] In the example below, what permissions does the group that owns the file have?

```
d rwx rw- r--
^ ^   ^   ^
| |   |   └──  other
| |   └──  group
| └──  user
└── file type (d indicates a directory)
```

Permissions have two ways of being represented: 
- symbolic representation (rwx)
- numeric (or octal) representation (0-7)

**Permissions table:**

| Pattern | Effective permission | Decimal representation |
| --- | --- | --- |
| --- | None | 0 |
| --x | Execute | 1 |
| -w- | Write | 2 |
| -wx | Write and execute | 3 |
| r-- | Read | 4 |
| r-x | Read and execute | 5 |
| rw- | Read and write | 6 |
| rwx | Read, write, execute | 7 |

### Changing file permissions with `chmod`

To change file permissions you can use the `chmod` utility.

`chmod` commands can be written using the symbolic method or the numeric method.

```bash
chmod 644 <file-name> 
chmod u=rw,g=r,o=r <file-name>
```

The symbolic method uses a combination of values that represent permissions and scope and operations that represent the change you want to make. 

```
- '+' add a permission
- '-' remove a permission
- '=' set a permission explicitly
```

`chmod u+x file` will add execute permissions to the user.

`chmod a+x file` will add execute permissions to the user, group and other

`chmod u+x,g+w,o-r file` will add execute to the user, write to the group and remove read from other.

`chmod u=rw,g=r,o= file` will set the users to read and write, groups to read and other to nothing.

### changing file ownership with `chown`

The `chown` command can be used to change the user or group owner of a file

```
chown [OPTIONS] USER[:GROUP] <file>
```

Examples:

```bash
chown user-name:group-name file-name
# change both user and group
chown user-name file-name
# this will do the same thing as the above command
```

```bash
chown :group-name file-name
# change only the group owner of a file
```

```bash
chown -R www-data: /var/www
# -R for recursive
```

**Reference:**

[Linuxize chown](https://linuxize.com/post/linux-chown-command/)

**Additional**

- s = setUID or SetGID permissions
- t = sticky bit (special permissions)

SetUID and SetGID can be used to set a files permission so that any user has the permissions of the User SetUID, or the Group SetGID of that file.

Example:

```bash
sudo chmod u+s file
```

Would allow any user to interact with a particular file as if they were the root user.

The sticky bit can  be used to set a file so that it only the files owner, or root can delete the file or rename the file.

```bash
chmod +t <directory-name>
```

To view setUID and sticky bit permissions use `ls -ld`


## Lab week 4

Create a new user and configure the ssh server so that we can no longer connect to our server via root.

### Step one, create a new user

This new user should be able to use sudo to temporarily elevate their privileges so that you can perform administrative tasks with the user.

The user should have a password. 

The user should also use bash as their login shell, since we will be interacting with the user in the bash shell in class.

Don't use "user" or "test-user" for a user name. 

### Step two, make it possible for your new user to connect to the server via ssh

Copy the .ssh directory from the root users home directory to the new users home directory (including any files in the directory). 

Change ownership of the directory, and files in the directory so that the copy in your new users directory is owned by the new user and the new users primary group. 
### Step three, test that you can connect to your server with your new regular user

You can do this by replacing 'root' with your new user-name in the ssh command you use to connect to your server.

### Step four,  edit ssh configuration so that the root user can no longer connect to the server via ssh

- [?] Where do you think we should look for the ssh configuration files?

The file we are looking for is the `sshd_config` file.

Open the file in vim. You will want to use sudo for this.

Look for the line `PermitRootLogin yes` and change yes, to no.

- [?] How could you find that line in vim a little faster?

Save the file and restart the ssh service. We will look at services and `systemctl` in another class. You can do this with the following command:

```bash
sudo systemctl restart ssh.service
```

### Step five, test that you can no longer connect to your server as root.

You should get a permission denied error

Moving forward you should always work on your Debian server as a regular user. So if you create a new droplet perform these steps again.

## Reading

From Ch 8, The Bourne Again Shell (bash) of [A Practical Guide to Linux Commands, Editors, and Shell Programming, Fourth Edition](https://learning.oreilly.com/library/view/a-practical-guide/9780134774626/ch08.xhtml#ch08)

Read the following sections:
- Startup Files
- Processes
- Processing the command line

## Practice

Create a new user with `useradd`
- Your new user should have bash set as their login shell
- Your new user should have a home directory in /home that contains the contents of `/etc/skell`

Give your new user a password

Add your new user to the sudo group, so that they can use sudo

==After this stage you should be able to perform the following as your new user, so switch to your new user.==

How could you demonstrate or test that your new user can use sudo?

Create a new group '2420-group'

Create a directory `/var/2420`

Make the new 2420 directory group owner the 2420-group group

Add your user to the 2420-group group

Create a file in the 2420 directory and change it's permissions so that it matches the following permissions. `-rw-r-xr-x`

How could you delete your user and group? 

### Practice questions

- [?] How many groups can a file belong to?

- [?] Can a user belong to more than one group?

- [?] The three permissions are...?

- [?] How can you add a user to a group?

- [?] Which file are user passwords stored in?
## todo
- [ ] Complete the reading before next weeks class
- [ ] Do the practice exercises in class
- [ ] Work on assignment 1