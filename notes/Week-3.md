---
tags:
  - BCIT
  - Linux
topics:
  - FSH
  - grep
  - find
---

# Week 3 2420 

# Learning outcomes and topics

This week:
- The Filesystem Hierarchy Standard
	- Identify the location of key directories
- create a symbolic link
- use find to find a file
- use grep to find a pattern in a file

Next week:
- Permissions, groups and users 

# The Linux filesystem

## What is a filesystem

> A _filesystem_ is the methods and data structures that an operating system uses to keep track of files on a disk or partition; that is, the way the files are organized on the disk.
> - Linux System Administrators Guide

We could think of this as the underlying rules for how data is stored. On Linux these are commonly things like [BTRFS](https://btrfs.readthedocs.io/en/latest/Introduction.html), [EXT4](https://kernelnewbies.org/Ext4)...

A filesystem is also the higher level organization of files on a system. This is what we are going to look at today.

**Reference:**

[Arch Wiki File system](https://wiki.archlinux.org/title/File_systems)
## Filesystem  Hierarchy Standard

![[standard-unix-filesystem-hierarchy.png]]

Most Linux distros have files and directories organized according to the Filesystem Hierarchy Standard. Probably the most notable deviation from this is [NixOS](https://nixos.org/).

The standard allows for users and developers to know where important files and directories are found on a system. 

The filesystem begins at the root directory `/` and branches out like a family tree.

You can find out more using the man page`man hier`

**Reference:**

[Filesystem hierarchy standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

[Arch Wiki filesystem hierarchy](https://man.archlinux.org/man/file-hierarchy.7.en)

## root `/`

The root directory, `/` is the start of our tree, or the beginning of our directory structure.

Inside of the root directory are key directories used to store specific files on your system.

## boot `/boot`

`boot` contains static files for the boot loader.

`boot` stores data that is used before the kernel begins executing user-mode programs.

## dev `/dev`

`dev` contains special, or device files 

## etc `/etc`

The `etc` directory contains configuration files

## bin `/bin` -> `/usr/bin`

The `/bin` directory historically stored utilities needed to run in single user mode. Now bin is generally a symbolic link that points to /usr/bin

## proc `/proc`

The directory /proc contains (among other things) one subdirectory for each process running on the system, which is named after the process ID (PID).

proc is a pseudo filesystem

**reference:**

https://www.kernel.org/doc/html/latest/filesystems/proc.html

## var `/var`

contains variable data files. `/var/log` for example
## home `/home`

Regular users home directories are stored here. For example, if you have a used named kim, kim's home directory would be in `/home/kim`

## pseudo filesystem

A pseudo filesystem is a way to work with non-file data as though it were a file.

The advantage to this method is that organizing and interacting with this data can be handled in the same, familiar way that you work with files.

# symbolic links

> In addition to hard links, Linux supports _symbolic links,_ also called _soft links_ or _symlinks._ A hard link is a pointer to a file (the directory entry points to the inode), whereas a symbolic link is an _indirect_ pointer to a file (the directory entry contains the pathname of the pointed-to file—a pointer to the hard link to the file).
> - A Practical Guide to Linux Commands, Editors and Shell Programming

**Advantages of symbolic links**

You cannot create a hard link to a directory, but you can create a symbolic link to a directory.

**Dereferencing symbolic links**

when you dereference a symbolic link you follow the link to the target file

**Creating a symbolic link**

You can create a symbolic link with the `ln` utility using the `-s` option.

```bash
ln -s [OPTIONS] FILE LINK
```

```bash
ln -s source_file symbolic_link
```

**Viewing links with `ls`**

When you use the the `ls` utility with the `-l` option links will appear with an arrow pointing to the file they link to. In the example below 'bin' is a link that points to 'usr/bin'

![[Screenshot from 2023-09-18 11-37-11.png]]

**Reference:**

[`ln` command in Linx](https://linuxize.com/post/how-to-create-symbolic-links-in-linux-using-the-ln-command/)
# Finding files with `find`

The `find` utility can be used to find files based on file properties. The name of the file, the size of the file, when the file was last modified...

```bash
find [options] [path...] [expression]
```

## -exec

`-exec <command> {} \;` use this to execute a command on files found by find
`-exec <command> {} +` same as above, but collect files, then run command. This option will involve fewer invocations of the command.
## examples

`find ~/ -type f` find all of the regular files in your home directory

`find . -type f -name "*.txt"` 
find all files in the current directory, or sub directory that end in ".txt".

`find . -type f -mmin -10` 
find all of the files in the current directory that were modified in the last 10 minutes (less than 10 minutes ago)

`find . -type f -mmin +10` same as above, but will find files modified more than 10 minutes ago.

`find / -size +10M` find files larger than 10 Megabytes anywhere in the root directory or sub-directories. 

**-ctime vs -mtime**
- -mtime is the file contents.
- -ctime is file metadata and contents, permission for example.
## practice

bonus, try using the man page for `find` to help you.

You might want to create a practice directory for this, `mkdir week3`

After you cd into your new practice directory, copy and paste this command into the terminal and hit enter.
This will create two directories containing .py and .pyc files.
```bash
mkdir dir{1..2}

for dir in $(find . -mindepth 1 -type d); do
  touch $dir/f{1..4}.pyc $dir/p{1..4}.py
done
```

What does the find command do in the script above?

Write a find command that finds all of the .py files you just created.

Write a find command that will find and delete all of the .pyc files

Write a find command that will find all of the files that were accessed in the last 24 hours.

-name is case sensitive, how could you do a search for names that isn't case sensitive?

**Reference:**

[find command in Linux](https://linuxize.com/post/how-to-find-files-in-linux-using-the-command-line/)
# Finding files with `grep`

`g/re/p` (_**g**lobal / **r**egular **e**xpression search **/** and **p**rint_)

The `grep` utility can be used to find patterns of text in a file(s). 

```bash
grep [_OPTION_...] _PATTERNS_ [_FILE_...]
```

This has a few uses, an obvious one is looking for a file based on the contents of that file. 

Perhaps a less obvious use of grep is to *filter* the output of another command. You can do this with the pipe character.

`ps -eo comm,rss | grep bash`

## examples

Remember that most server versions of a Linux OS will have minimal features. One of the niceties that most work station Linux OSs have is colored output for grep. You can achieve this with `--color=always`. See the man page for grep for more.

`grep search-word file` basic grep command

`grep -iw search-word file` 
search only for the search word, not sub-strings, and ignore case

`grep -r search-word directory` 
search recursively for all instances in a file.
-r and -R are slightly different, see the grep man page for more

`grep -C 2 "search words" file`
return found instances of a search term with 2 lines above and below where the search pattern was found, for context.

`grep -c "set" ~/.bashrc` 
count the number of times "set" is found in our .bashrc file

`grep -rl "search pattern" dir`
return a list of files that contain the search pattern

`fc-list | grep -i "nerd"`
find all of the nerd fonts you have installed on your system
uses a pipe to filter the output of the `fc-list` command. 

### regular expressions

You will probably learn more about regular expressions in one of your programming courses. Regular expressions can get gross. I recommend finding a good cheat sheet and add that to your notes.

`grep -P "\d{2}" file` 
this uses the -P option which tells grep to use a perl compatible regular expression. Not all versions of grep have this option.

`grep "[0-9]{2}" file` will do the same as above

`grep -P '\(\d{3}\) \d{3}-\d{4}|\d{3}-\d{3}-\d{4}' file` 
This will match phone numbers written as:
(555) 555-5555 or 555-555-5555
in a regular expression '|' is or

## practice

copy the text below into a file

```
alias             name          age  city           ocupation  species
batman            Bruce Wayne   54   gotham         none       human
superman          Clark Kent    43   metropolis     reporter   kryptonian
supergirl         Kara Danvers  28   national city  reporter   kryptonian
raven             Rachel Roth   22   washington dc  teacher    half demon 
blue beetle       Jaime Reyes   21   palmera city   student    Human
wonder woman      Diana Prince  5000 washington dc  nurse      demigod
martian manhunter John Jones    350  national city  detective  martian
zatana            zatana zatara 37   Washington dc  magician   human
```

use grep to find all of the reporters

use grep to find all of the humans

use grep to find all of the people from washington dc

use grep to find all of the people in their twenties

find all of the errors in any file in the `/var/log` directory

**Reference:**

[Where GREP came from](https://www.youtube.com/watch?v=NTfOnGZUZDk) 

[grep command in Linux](https://linuxize.com/post/how-to-use-grep-command-to-search-files-in-linux/)
# Reading

[Ch 6, Managing Users and Groups](https://learning.oreilly.com/library/view/linux-administration-a/9781260441710/ch6.xhtml#ch6), from [Linux Administration: A Beginner's Guide, Eighth Edition, 8th Edition](https://learning.oreilly.com/library/view/linux-administration-a/9781260441710/)


# todo
- [ ] complete course reading
- [ ] complete the practice quiz on D2L
- [ ] work on your projects in your groups