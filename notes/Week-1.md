---
tags: 2420, BCIT
topics: Intro, DigitalOcean, Course overview
---
# Week 1 2420

## Learning outcomes and topics

**This week:**
- Introductions
- Brief course overview
- Linux, what, why, where and who
- Intro to DigitalOcean
	- What is a cloud service provider
	- Create a "droplet" in DO
	- Connect to your droplet using ssh

**Next week:**
- Working with files in the command line
- get help in the command line
- intro to Vim

## Introductions

**About me**

I Started using a Linux OS as my daily driver when I was in high school.

I started teaching at BCIT in 2020

Most of my professional career has involved a lot of work in Linux
- SysAdmin
- DevOps

Distros I regularly use on Desktop/Laptop and servers:

- openSUSE Aeon (formerly MicroOS)
- Fedora Silverblue
- Alpine Linux

Outside of work
I like most of the usual nerdy things, Sci-fi, comics...
I also like climbing

![falling](attach/falling.jpg)


**About you**

What is your name?

Do you have any previous experience with a Linux OS (or an OS that isn't Windows or Mac)?

Scale from 1-10 how comfortable do you feel working in the command line?

Outside of your school work what do you like to do? One of your primary hobbies.

## A brief course overview

### Office hours

Monday 08:30-10:30 Location TBD 
Wednesday 11:30-12:30 Location TBD 
Thursday: 08:30-10:30 Location TBD 
Please schedule at least 24 hours in advance via email or discord

### Course breakdown

Assignments 50%
- Research Video
- Shell Scripting 
- Server setup
Midterm exam 25%
Final exam 25%

There are also readings and lab exercises to help you learn the material. It is **highly recommended** that you do these, even though they are not graded items

The rest of the course outline is available [here](https://www.bcit.ca/outlines/acit2420/). This may change slightly as we progress through the course.

### Some general tips for success

#### Take notes and stay on top of the work

The lab and other practice material is not graded material. This doesn't mean that this material is optional. Work done in the labs will help you with the assignments, as well as the exams. Similarly doing the reading every week and taking notes on the readings, as well as in class will help you learn the material, which will help you complete the assignments and exams.
#### ChatGPT and AI tools like Copilot

**Option A**
Just don't use AI tools for your school work. 

**Option B**
For some reason you can't do Option A.
Limit yourself to using AI with a really specific set of boundaries. Write queries like you are asking one of your instructors a question.
#### How to take notes

Earlier I mentioned that taking notes is really important. It is, can't emphasise this enough. If you are currently getting Bs and you want to get As, learn to take better notes. 

A few note taking tools you may want to look at:
- [Logseq](https://logseq.com/)
- [Obsidian](https://obsidian.md/)
- [Remnote](https://www.remnote.com/)
- Pen and paper(try it out)

If later in the class you have questions about this or want more help in getting better at taking notes, just let me know.

## Linux: what, why, where and who

### What is Linux?

Before moving on I have a short form that I would like you to respond to.

https://tally.so/r/wbj8d2

### The Linux kernel

![[Drawing 2023-08-16 21.35.58.excalidraw]]

### Linux distributions

So if Linux is technically just the kernel, what is a Linux OS? Or how do I actually use Linux on my computer.

A Linux OS is a "distro". This is the Linux kernel, along with other tools that are needed to make up a complete operating system. A Linux OS might include:
- an init system (systemd)
- command line tools, or utilities (gnu, busybox)
- A c library (libc, musl)
- A desktop environment (Gnome)
- a boot loader (grub2, systemd-boot)

The Linux distro that we will be using this term is [Debian](https://www.debian.org/)

Linux distros
![linux map](attach/2023_Linux_Distributions_Timeline.svg)


### Where is Linux being used

The short answer is everywhere, even on Mars. 

I bet that most of you wrote "servers" in the form you filled out earlier. And you are correct, more than 95% of the internet runs on a Linux OS.

Android uses the Linux kernel, so most of the worlds smart phones.

Touch screens in vehicles, Ford, Tesla...

Smart appliances

And, of course, personal computers.

### Why is Linux so popular

Many of you are probably guessing, price, it is free! This is absolutely an important factor.

However, the main reason is flexibility. 

Have a look at the list of components that might make up a Linux OS. All of that is optional, some of them are larger categories that contain several smaller pieces.  There are alternatives for everything in that list. Don't like Gnome, try KDE, or Sway.

A Linux OS can be highly configured to suit a particular task. 

Linux also runs on a lot of different hardware. 

References:
The map came from https://en.wikipedia.org/wiki/Linux_distribution
https://developer.ibm.com/articles/l-linux-kernel/
https://www.pcmag.com/news/linux-is-now-on-mars-thanks-to-nasas-perseverance-rover

### who created Linux, and who is maintaining it now?

Linus Torvalds created in Linux in 1991, when he was an undergrad. 
Linus also created Git; to work on the Linux kernel.

![linus](../attach/linus-torvalds-7.webp)

Today Linus still oversees development of the Linux kernel, however it is an open source community project developed by thousands of people and organizations. 

References:
[A brief history of Linux](https://www.digitalocean.com/community/tutorials/brief-history-of-linux)

## Intro to DigitalOcean

### DO what and why

DigitalOcean is a cloud service provider, similar to AWS and GCP. 

DigitalOcean has always had a developer focused approach, instead of a business focused approach. In practical terms this means that they offer fewer products, and the products that they do offer are (IMO) easier to use than many similar tools.

This makes it a good choice as an introductory cloud service provider.

### Droplets

One of the services that DO offers is a VPS (virtual private server) which is just a VM running on a remote server. DO calls these "droplets", in keeping with their ocean theme.

These droplets are going to be the Linux environment that we use this term. Along the way we will look at a few other tools offered by DO.

References:

https://www.digitalocean.com/
https://www.digitalocean.com/products/droplets

### Get some free stuff 

As a student you can get a $200 credit for DigitalOcean using the GitHub student developer pack. https://education.github.com/pack

This should be more than enough to cover the cost of using DigitalOcean this term.

### SSH (Secure Shell)

SSH is the most common way to issue commands to a remote service. In addition to using SSH to connect to the droplets that we are about to setup, you can use SSH to connect to remote Git services (GitLab, GitHub...) and other services that you will use as developer.

![ssh](../attach/SSH_simplified_protocol_diagram-2.webp)

To use SSH we need to create a key pair, you can do this with the `ssh-keygen` utility.

**Create an SSH key pair**

To create an ssh key pair on your local machine, run the command below in the terminal. This should work in Windows and MacOS. The [openSSH](https://www.openssh.com/) client is installed on MacOS, and is probably installed on your Windows machine too. 

First create a new .ssh directory, if you get an error telling you that this already exists don't worry about it
`mkdir .ssh`

After you have created the new directory create your new ssh key pair.
`ssh-keygen -t ed25519 -f .ssh/do-key -C "your-email-address"`

After you have created a key pair the private key stays on your local machine and the public key (the one that ends in .pub) can be copied to your server.

We will do this after everyone has a DigitalOcean account.

References:
[Linkedin Learning Learn SSH](https://www.linkedin.com/learning/learning-ssh-14571185?u=2097252)
[How Secure Shell works, Computerphile](https://www.youtube.com/watch?v=ORcvSkgdA58)
### Creating a DO account and your first droplet

You will need a credit card to create a DO account. If you don't have one follow along with a friend for today. 

DO accounts are free, you only pay for the resources that you use. So if you want to you can destroy droplets and create new ones as needed.

### Adding your SSH key to your DigitalOcean account

Go to the Settings -> Security menu in your DO console and click the "Add SSH Key" button.

If you are using PowerShell in the windows terminal you can copy your ssh public key like this. Be sure to replace user-name and your-key with your actual user name and the name of the key you just created.
`Get-Content C:\Users\user-name\.ssh\do-key.pub | Set-Clipboard`

In MacOS use `pbcopy`
`pbcopy < ~/.ssh/your-key.pub`

![[Screenshot from 2023-09-04 19-55-55.png]]

![[Screenshot from 2023-09-04 19-55-26.png]]

### Create a droplet

I will go through this in class, if you missed class follow along with the official [docs](https://docs.digitalocean.com/products/droplets/how-to/create/#create-a-droplet-in-the-control-panel) and get in touch if you need any help. Because this is new to everyone, we will probably go over this again in week 2 as well. So don't panic.

### Connect to your new droplet using your ssh key

After you have created your droplet you should see something like the image below.

![[Screenshot from 2023-09-04 20-13-39.png]]

'146.190.42.230' is the IP address for the server in the image. You are going to use this to connect to your server.

use the `ssh` utility, specify the path to your key, the private one, the user, currently root is the only user on the droplet you just created, and the IP address of the droplet.

The command will look a little like this: 
`ssh -i path-to-your-key root@146.190.42.230`

The first time you do this you will need to confirm that you want to connect to your server. Type "yes" and press enter.

## Reading

Reading material should always be completed before the start of next weeks class.

On your Debian server Vim and the man utility will already be installed, so you don't need to install anything.

[Learning Vim](https://www.linkedin.com/learning/learning-vim?u=2097252), Section 1. Basics 
[Locate, read, and use system documentation](https://www.linkedin.com/learning/red-hat-certified-system-administrator-ex200-cert-prep-1-deploy-configure-and-manage/locate-read-and-use-system-documentation?autoplay=true&resume=false&u=2097252) from Red Hat Certified System Administrator


## Labs, practice and study material

Try creating, connecting to and destroying a few more droplets.

When you are learning to use Vim, try to learn a few new things every week. Start with the basics, movements, copying and pasting text... Then look for more efficient ways to perform a given task. Pick something that you do often. After this you might want to look into plugins, and ways to customize Vim. The goal of the course isn't that everyone become a Vim wizard by the end, but you should be able to make small edits to configuration files on a server using vi or Vim comfortably.

![[p2hrwV6ydTvIU2-XoCZedKnvgXpF4nrPgxDCaYtL7Mc.webp]]
### Create a new droplet and connect to it via SSH

[create a DO droplet video](https://www.loom.com/share/8e4c950161604496b3c0e6c611a3cfa6?sid=a30ec481-a193-488a-9fe6-1383df7f0b5b)


## Todo

- [ ] Complete the reading, don't forget to takes notes
- [ ] Destroy the droplet that you created in class
- [ ] Create a new droplet for next weeks class
- [ ] Try using the `man` command to find out what the options do in the `ssh-keygen` command you used to create your ssh key pairs in class.