---
tags:
  - "2420"
  - BCIT
topics:
  - man
  - vim
---
# Week 2 2420 

# Learning outcomes and topics

This week:
- if needed review creating a droplet in DO
- navigating in the commandline
	- create a new directory and change into it
	- identify where you are in the command line
- use a man page to find information about a utility (note I will likely use, command, utility interchangeably)
- find a utility with the man utility
- use vim to create a new file
- use vim to edit a file

Next week:
- The Linux filesystem FSH

# Navigating in the command line

Whether you are a software developer working on applications for Microsoft or a Site Reliability Engineer working for [HashiCorp](https://www.hashicorp.com/), you are going to spend a lot of time working in the command line. So getting more comfortable with working in a text based environment is important.

When you get more comfortable working in the command line you will find yourself using it more often, even when you don't have to. Working in the command line is often faster than working in GUI, and is unquestionably more powerful.

## The prompt

Let's start with what you see when you open the terminal.

`username@hostname ~$` The default prompt in Linux generally looks a little like this.

This shows us who we are, which user, where we are, the hostname of the machine we are connected to and where we are `~` is a shortcut for the current users home directory(more on this next week).

`$` vs `#` generally the `#` is used to indicate that you are root and a `$` a regular user.


## utilities

`cd` 

is cd a shell builtin?

`mkdir`

Which option would you use to create nested directories?

`pwd`

`ls` 

Which option would you use to show hidden files?
Which option would you use to show the long list format?

`cp`

`rm`
be careful with `rm` It doesn't move things to the trash, it permanently deletes them.

`mv`

## Tips for working in the command line more efficiently

**Use the tab key**

Start typing a command and press tab to complete the command faster

**Move the cursor in a command**

beginning-of-line `ctrl+a`
Move to the start of the current line.

end-of-line `ctrl+e`
Move to the end of the line.

forward-char `ctrl+f`
Move forward a character.

backward-char `ctrl+b`
Move back a character.

forward-word `meta+f`
Move forward to the end of the next word.  Words are composed of alphanumeric characters (letters and digits).

backward-word `meta+b`
Move back to the start of the current or previous word.  Words are composed of alphanumeric characters (letters and digits).

**Delete a command, from cursor to the line beginning**

This one is handy when you realize you are typing the wrong command.
`ctrl+u`

**Make use of the shells history search feature**

This one is handy when you are running the same command multiple times.
`ctrl+r` and search for a previous command,

You can find more of these using the man page for bash `man bash` and search for 'Commands for Moving'



## Special characters

The tilde `~` this is a short cut to the current users home directory

the star `*` is used as a wildcard, this matches any pattern.

## Practice

How could you make the `rm` command a little safer? This will require you to use the man pages

Can you copy a directory? How?

Assume that you have 10 .otf files in a directory, and you want to copy all of them to a new directory. How could you do this faster than doing one at a time?

# Getting help from the man pages

## Pagers

When you open a man page you are opening the manual entry in a pager, `less`. A pager, shows you a page of info at a time.

- To find out how to use less press `h`
- To quit press `q`
## reading a man page

there is a man page for man, to help us read man pages `man man`

   **bold text**         type exactly as shown.  
   *italic text*        replace with appropriate argument.  
   [-abc]             any or all arguments within [ ] are optional.  
   -a|-b              options delimited by | cannot be used together.  
   argument ...       argument is repeatable.  
   [expression] ...   entire expression within [ ] is repeatable.

## Man page sections

   1   Executable programs or shell commands  
   2   System calls (functions provided by the kernel)  
   3   Library calls (functions within program libraries)  
   4   Special files (usually found in /dev)  
   5   File formats and conventions, e.g. /etc/passwd  
   6   Games  
   7   Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7), man-pages(7)  
   8   System administration commands (usually only for root)  
   9   Kernel routines [Non standard]  

Sometimes a man page of the same name can be found in multiple sections, in this case you may need to specify the section to see the correct man page.

for example `man passwd` and `man passwd.5` will take you to different manual pages.
## Headings of a man page

Not all of these headings will be in every man page

- **Name:** The name of the command the man page is describing.
- **Synopsis:** A summary of the command and its syntax.
- **Configuration:** Configuration details for a device.
- **Description:** An explanation of what the program does.
- **Options:** A description of the command-line options the command accepts.
- **Exit Status:** Possible exit status values for the command, and what might cause them to be used.
- **Return Value:** If the man page is for a library routine, this describes the value the library routine can send back to the function that called that routine.
- **Errors:** A list of the values that might be placed in `errno` [in the event of an error](http://man7.org/linux/man-pages/man3/errno.3.html).
- **Environment:** A list of the environment variables that affect the command or program, and in what way.
- **Files:** A list of the files the command or program uses, such as configuration files.
- **Attributes:** A summary of various attributes of the command.
- **Versions:** Details of the Linux kernel or library versions where a system call or library function first appeared or changed significantly from previous versions.
- **Conforming to:** A description of any standards with which the command might comply, such as [POSIX](https://en.wikipedia.org/wiki/POSIX).
- **Notes:** Miscellaneous notes.
- **Bugs:** Known issues.
- **Examples:** One or more examples demonstrating the use of the command.
- **Authors:** The people who wrote or maintain the command.
- **See also:** Recommended reading related to the command or topic.

## Searching for a man page

   `man -k printf` 
   Search the short descriptions and manual page names for the keyword printf as regular expression.  Print out any matches.  Equivalent to `apropos printf`.


## Searching for text in a man page

To search for text inside of a man page use `/` 
`/search-word`

You can even search for multiple words
`/Commands for Moving` 

Remember that the default pager on most Linux distros is less, this is a less feature. It is also a Vim feature

References:
[Arch wiki man page](https://wiki.archlinux.org/title/Man_page)


## Practice

Try searching for a utility. Let's assume that you don't know how to change a users password. How could you find a utility to change a users password using `man`?

Look at last weeks notes, find the command that we used to create SSH keys. Use the man page for that utility to find out what the options are.

Try using the man page for less to find out how to go to the beginning of file. What is an easier way to do this, without leaving the terminal?

Use the man page for the `uname` utility.
What does the `uname` utility do?
How could you print just a kernel release?

# Editing files in Vim

## What is Vim

Vim (v i improved) is a text editor. 
Don't let the default looks fool you, anything VSCode can do Vim can do.

vi (pronounced v i) is a text editor created by Bill Joy in 1976

In 1992 Bram Moolenaar released Vim. Vim was designed to be a full featured, modern text editor based on vi. Bram continued to be the primary developer of Vim until his death in August of this year. 

## Why should you learn to use vi/Vim/Neovim.

Some version of vi or Vim is generally included in the software of most Linux based server OSs. Sometimes they are the only text editor installed. Last week I mentioned that Vim is the default text editor used by Git. At some point you will have to use Vim, it will be nice for you if you know how to when you do.

In addition to the fact that you will have to, you might want to. Millions of software developers use (Neo)Vim as their text editor because of how Vim can be customized to suite individual user needs and how efficiently they can edit code with it. Every other editor has some sort of Vim emulation.

## Modal editors

A modal editor is an editor, with; well different modes. This is what makes Vim so efficient.

Most of your time working in a text editor isn't spent writing new code, it is spent editing and reading existing code. Because of this Vim, by default opens in "Normal" mode. Which is Vim's primary editor mode.

### Other modes:

**Insert** mode, this is the mode you use when you want to write text. Enter insert mode by pressing "i" in normal mode.

**Visual** mode, this mode is used to make text selections. Enter visual mode by pressing "v" in normal mode. Visual mode technically has three settings:
regular, select by character
line, select by line
block, select by block

**Command** mode, this mode is used to enter commands, such as writing the contents of a buffer to a file(saving a file). In normal mode use ":" to enter command mode

==Return to normal mode from other modes by pressing the escape key.==

Depending on the tutorial you look at you will see other modes, Ex, Replace... But I would argue Ex commands are part of command mode, and Replace is part of Normal. The point is that having different, specialized modes can make editing text more efficient.

**Other modal text editors:**

[Helix](https://helix-editor.com/)

[Kakoune](https://kakoune.org/)

## Vim basics

### open a file in Vim

`vim file-name`

or `vim` then inside of vim `:e file-name`

### save a file in Vim

`:w` 

### close Vim

`:q` 
`:q!` to quit without saving

`ctrl+z` does not quit Vim

You can also save and quit `:wq`

### Some basic Vim movements

`h` `j` `k` `l` right, down, up, left
`w` `b` next word, previous word
`0` `$` start of line, end of line

## Vim Cheat Sheet

Cursor movement (Normal/Visual Mode)

- `h` `j` `k` `l` - Arrow keys
- `w` / `b` - Next/previous word
- `W` / `B` - Next/previous word (space seperated)
- `e` / `ge` - Next/previous end of word
- `0` / `$` - Start/End of line
- `^` - First non-blank character of line (same as `0w`)

Editing text

- `i` / `a` - Start insert mode at/after cursor
- `I` / `A` - Start insert mode at the beginning/end of the line
- `o` / `O` - Add blank line below/above current line
- `Esc` or `Ctrl+[` - Exit insert mode
- `d` - Delete
- `dd` - Delete line
- `c` - Delete, then start insert mode
- `cc` - Delete line, then start insert mode

Operators

- Operators also work in Visual Mode
- `d` - Deletes from the cursor to the movement location
- `c` - Deletes from the cursor to the movement location, then starts insert mode
- `y` - Copy from the cursor to the movement location
- `>` - Indent one level
- `<` - Unindent one level
- You can also combine operators with motions. Ex: `d$` deletes from the cursor to the end of the line.

Marking text (visual mode)

- `v` - Start visual mode
- `V` - Start linewise visual mode
- `Ctrl+v` - Start visual block mode
- `Esc` or `Ctrl+[` - Exit visual mode

Clipboard

- `yy` - Yank (copy) a line
- `p` - Paste after cursor
- `P` - Paste before cursor
- `dd` - Delete (cut) a line
- `x` - Delete (cut) current character
- `X` - Delete (cut) previous character
- `d` / `c` - By default, these copy the deleted text

Exiting

- `:w` - Write (save) the file, but don't quit
- `:wq` - Write (save) and quit
- `:q` - Quit (fails if anything has changed)
- `:q!` - Quit and throw away changes

Search/Replace

- `/pattern` - Search for pattern
- `?pattern` - Search backward for pattern
- `n` - Repeat search in same direction
- `N` - Repeat search in opposite direction
- `:%s/old/new/g` - Replace all old with new throughout file ([gn](http://vimcasts.org/episodes/operating-on-search-matches-using-gn/) is better though)
- `:%s/old/new/gc` - Replace all old with new throughout file with confirmations

General

- `u` - Undo
- `Ctrl+r` - Redo

More cursor movement

- `Ctrl+d` - Move down half a page
- `Ctrl+u` - Move up half a page
- `}` - Go forward by paragraph (the next blank line)
- `{` - Go backward by paragraph (the next blank line)
- `gg` - Go to the top of the page
- `G` - Go the bottom of the page
- `: [num] [enter]` - Go to that line in the document
- `ctrl+e / ctrl+y` - Scroll down/up one line

Character search

- `f [char]` - Move forward to the given char
- `F [char]` - Move backward to the given char
- `t [char]` - Move forward to before the given char
- `T [char]` - Move backward to before the given char
- `;` / `,` - Repeat search forwards/backwards

File Tabs

- `:e filename` - Edit a file
- `:tabe` - Make a new tab
- `gt` - Go to the next tab
- `gT` - Go to the previous tab
- `:vsp` - Vertically split windows
- `ctrl+ws` - Split windows horizontally
- `ctrl+wv` - Split windows vertically
- `ctrl+ww` - Switch between windows
- `ctrl+wq` - Quit a window

## Configuring Vim

Vim configuration is generally in the ~/.vimrc file and ~/.vim/ directory

here is a good starter vimrc copy and paste this into a new `~/.vimrc` file

```vim
" Comments in Vimscript start with a ".
" create a new file ~/.vimrc and copy this code block into it.
" you can create the file by entering the command 'vim ~/.vimrc'
" Vim is based on Vi. Setting `nocompatible` switches from the default
" Vi-compatibility mode and enables useful Vim functionality.
set nocompatible

" Turn on syntax highlighting.
syntax on

" Disable the default Vim startup message.
set shortmess+=I

" Show line numbers.
set number

" This enables relative line numbering mode.
" with relative and regular the current line will show the actual line number
set relativenumber

" Always show the status line at the bottom, even if you only have one window open.
set laststatus=2

" use the system clipboard instead of buffer
" depending on the distro you may need to install gvim, or vim-gtk to get this feature
" you can check that you have a version of vim capable of this by running this command 'vim --version | grep clipboard'
" if you see +clipboard it will work, if not it won't.
set clipboard=unnamedplus

" The backspace key has slightly unintuitive behavior by default. For example,
" by default, you can't backspace before the insertion point set with 'i'.
" This configuration makes backspace behave more reasonably, in that you can
" backspace over anything.
set backspace=indent,eol,start

" replace tabs with spaces
set expandtab

" 1 tab = 2 spaces
set tabstop=2 shiftwidth=2

" By default, Vim doesn't let you hide a buffer (i.e. have a buffer that isn't
" shown in any window) that has unsaved changes. This is to prevent you from "
" forgetting about unsaved changes and then quitting e.g. via `:qa!`. I find
" hidden buffers helpful enough to disable this protection. See `:help hidden`
" for more information on this.
set hidden

" Do not save backup files. and swap files
" This might save you a lot of headache later.
" If you quit vim incorrectly it leaves a swap file, 
" like a backup of unsaved changes. This is a good thing, but more often than
" not these swapfiles cause problems for peopel new to vim.
set nobackup
set noswapfile
" alternatively you could save them in a different location, 
" just uncomment the lines  below and comment out the nobackup and noswapfile above 
"set backupdir=~/.vim/backup//
"set directory=~/.vim/swap//
"set undodir=~/.vim/undo//

" This setting makes search case-insensitive when all characters in the string
" being searched are lowercase. However, the search becomes case-sensitive if
" it contains any capital letters. This makes searching more convenient.
set ignorecase
set smartcase

" search
set hlsearch     " Highlight all search results
set incsearch    " Searches for strings incrementally

" Use the mouse, you actually usually don't want this.
" You are better off learning to use Vim without using the mouse
" You will be faster and more productive without it.
" If you really want to use the mouse uncomment the line below.
"set mouse=a

" enable wildmenu, a little menu that displays files and commands in the button left corner
set wildmenu
set wildmode=list:longest,full

" Unbind some useless/annoying default key bindings.
nmap Q <Nop> " 'Q' in normal mode enters Ex mode. You almost never want this.

" Disable audible bell because it's annoying.
set noerrorbells visualbell t_vb=

" Try to prevent bad habits like using the arrow keys for movement. 
nnoremap <Left>  :echo "Use h"<CR>
nnoremap <Right> :echo "Use l"<CR>
nnoremap <Up>    :echo "Use k"<CR>
nnoremap <Down>  :echo "Use j"<CR>

```


**Note:** if you want to try Neovim there are 100s of youtube videos on configuring Neovim, I would watch some of them


References:
https://www.redhat.com/sysadmin/beginners-guide-vim

# Assignment 1, Research video

More about assignment 1 can be found in the [[as1]] document in the assignments directory.

# Reading

Ch 4 from [A Practical Guide to Linux Commands, Editors, and Shell Programming 4th ed](https://learning.oreilly.com/library/view/a-practical-guide/9780134774626/ch04.xhtml#ch04)

Skip the ACL's: Access Control Lists section

Watch [Searching in files with grep](https://www.linkedin.com/learning/linux-system-information-and-directory-structure-tools-22666862/searching-in-files-with-grep?resume=false)

Watch [Find files with find](https://www.linkedin.com/learning/linux-system-information-and-directory-structure-tools-2017/find-files-with-find)

# todo

- [ ] complete course reading
- [ ] form a group and start talking about ideas for assignment 1