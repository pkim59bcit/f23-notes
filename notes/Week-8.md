---
tags:
  - bash
  - Linux
topics:
  - bash
  - functions
  - getopts
  - exit-status
---
# Week 8 2420  

# Learning outcomes and topics

This week:
- midterm notes
- Use the exit keyword to return an exit status when executing a script
- Encapsulate code in a function
- Use the case conditional
- Handle options in a shell script

Next week:
- Package managers

# midterm notes

- What is a Linux distro?
- What is a shell?

Vim. Make sure you are practicing and that you know the basics. You should try to use Vim for all of the text editing in this class. For example writing your scripts.

**Don't** use ctrl+c to exit insert mode. I don't know why the linkedin learning video recommends this. You didn't loose a mark for this, it is just worth mentioning again.
Use `<Esc>` or `CTRL-[`
Alternatively you can add a key binding, by adding something like this to your `~/.vimrc` file. The first line is a comment.

```vim
  "use jk to exit insert and visual mode
  inoremap jk <ESC>
```

**If you have 34/42 or higher well done!** keep doing what you are doing.
If you have a 25/42 or lower and there are not obvious things that you can do to improve your grades (the course readings, reviewing your notes...) You may want to talk to me (or another faculty member) to see if there are things you could do to improve your study techniques. 

# Exit status

The 'exit status' of a command indicates whether a command succeeded or not. In general an exit status of `0` indicates success and non-zero exit status indicates an error.

You can check the exit status of the previous command with `echo $?`

exit codes fall between 0 and 255

**examples:** 
If a command is not found, the child process created to execute it returns a status of 127. 

If a command is found but is not executable, the return status is 126.

The `exit` command will exit out of the current process and return an exit status which can be provided as an argument `exit 1`. 

You have been using `exit` all term to exit out of an ssh process.

When writing your own scripts you really only need to use 0 and 1.

```bash
#!/bin/bash

if [[ "$(whoami)" != root ]]; then
	echo "This script should be run with sudo"
	exit 1
fi
echo "doing stuff..."
exit 0
```

**Reference:**

[Bash Manual, exit](https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html)

# Functions

> Shell functions are a way to group commands for later execution using a single name for the group. They are executed just like a "regular" command. When the name of a shell function is used as a simple command name, the list of commands associated with that function name is executed.
> - Bash reference manual


```bash
# declare a function
funcex() {
	# the function body
	echo "hello"
}

# call a function
funcex
```

## local variables and returning values

The `local` keyword makes a variable local to the functions scope, by default variables are global.

```bash
#!/bin/bash

localretrn () {
	local func_result="some result"
	echo "$func_result"
}

func_result="$(localretrn)"
echo $func_result
```

## Using a function and a for loop to print all of the arguments to a script

```bash
#!/bin/bash

# This will print arguments to the function
printall() {
  for arg; do
    echo "arg to func: '$arg'"
  done
  echo "Inside func: \$0 is still: '$0'"
}
# This passes all positional parameters to the printall function
printall $@
```
## error function example

```bash
err() {
	echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
	exit 1
}  
# using the error function	
if ! do_something; then
	err "Unable to do_something"
fi
```

Both `$@` and `$*` are all positional parameters

`$*` All positional parameters (as a single word)
`$@` All positional parameters (as separate strings) 

- [?] Do local variables exist after a function has finished executing? 

**References:** 
- [Shell Functions](https://www.gnu.org/software/bash/manual/bash.html#Shell-Functions)
# Testing conditions with `case`

`case` can be used to test a value matches one of several possible options. 

You are testing to see if a variable, (`letter` in the example below) is one of several possible options.

The example below uses `read` to get input from a user.

```bash
#!/bin/bash

read -p "Enter a character " letter

case "$letter" in
  A)
    echo "You entered A"
    ;;
  B|b)
    echo "You entered B or b"
    ;;
  C)
    echo "You entered C"
    ;;
  [1-3])
    echo "You entered 1 2 or 3"
    ;;
  *)
    echo "You did not enter A, B, b, or C or 1, 2, or 3"
    ;;
esac
```

```
) indicates the end of the test, ie the pattern to try to match

;; indicates the end the condition

* Matches any string of characters. Use for the default case. 

? Matches any single character. Use this for the default character in getopts, where you are passing a single letter as a flag 

You can use a range [a-z] in a case statement.

| Separates alternative choices that satisfy a particular branch of the case structure.
```

In general you want to keep the logic in your case statements minimal. 

If you need to have additional logic, loops, conditions... it is generally cleaner to write a separate function.

# Handling options with `getopts`

getopts is a a shell builtin that can be used to aide you in handling options in your shell scripts.

let's start in the obvious place, read the help `help getopts` since getopts is a builtin this will display the help message.

getopts is a function that takes three arguments:

-   valid options that will be handled, ie `“a:b”` in the example below
-   a variable populated with the option arguments ie `opt` in the example below
-   a list of options and arguments to be processed `$@`

## `shift` positional parameters

`shift` will; shift positional parameters *n* characters to the left. The default is 1 character.

Save the file below and execute it like so `shift-examp 1 2 3 4`
the first line will print `1 2 3`
the second `2 3 4` shifted 1 position to the left
and the third `4` shifted 2 additional positions to the left 
```bash
#!/bin/bash

echo $1 $2 $3

shift

echo $1 $2 $3

shift 2

echo $1 $2 $3
```

## OPTARG

When a flag is set to expect an argument, the argument for that flag is held in the OPTARG variable. ie `my_script -n "ted"` the OPTARG for -n is the string ted.

The first colon in the options string below tells getopts to run in silent error checking mode. Or let the script define errors.

```bash
#!/bin/bash

vara=""
varb=""

while getopts ":a:b:" opt; do
  case "${opt}" in
    a)
      vara=${OPTARG}
      ;;
    b)
      varb=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument"
      exit 1
      ;;
    ?)
      exit 1
      ;;
  esac
done

if [[ $# -lt 1 ]]; then
  echo "you need to provide an option -a and or -b with an argument"
  exit 1
fi

if [[ -n $vara ]]; then
  echo "$vara"
fi

if [[ -n $varb ]]; then
  echo "$varb"
fi
```

## OPTIND

When dealing with a mix of getopts and positional arguments, **the flags and flag options should always be provided before the positional arguments!** This is because we want to parse and handle all flags and flag arguments before we get to the positional parameters.

The line below shifts the positional parameters to remove any options handled by getopts.

```bash
shift $(($OPTIND -1))
```

This also works if your options accept arguments. Try running the previous example with an additional positional parameter.

```bash
#!/bin/bash

vara=""
varb=""

while getopts ":a:b:" opt; do
  case "${opt}" in
    a)
      vara=${OPTARG}
      ;;
    b)
      varb=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument"
      exit 1
      ;;
    "?")
      exit 1
      ;;
  esac
done

shift $((OPTIND - 1))

# check if there are positional parameters
if [[ $# -lt 1 ]]; then
  echo "you need to provide an option -a and or -b with an argument"
  exit 1
fi

# print positional parameters
echo "$1"

if [[ $vara != "" ]]; then
  echo "$vara"
fi

if [[ $varb != "" ]]; then
  echo "$varb"
fi
```

## Example script, generate a random password

```bash
#!/bin/bash
#: Title       : rndpswd
#: Version     : 1.0
#: Description : generate a random password
#: Options
# Users can set a password length -l
# Users can add a special character -s
# Users can set vrbose mode with -v

#######################################
# Print a Usage message.
# Arguments:
#   None
# Outputs:
#   Write a help message
#######################################
usage() {
  echo "Usage: ${0} [-vs] [-l LENGTH]" >&2
  echo 'Generate a random password.'
  echo ' -l LENGTH specify the password length.'
  echo ' -s Append a special character to the password.'
  echo ' -v display output in verbose mode'
  exit 1
}

#######################################
# log additional messages
# locals:
#   MESSAGE
# Arguments:
#   message to log
# Outputs:
#   log message passed as argument
#######################################
log() {
  local MESSAGE="${@}"
  if [[ "${VERBOSE}" = 'true' ]]; then
    echo "${MESSAGE}"
  fi
}
# set a default password length
LENGTH=48

while getopts :vl:s OPTION; do
  case ${OPTION} in
    v)
      VERBOSE='true'
      log 'Verbose mode on.'
      ;;
    l)
      LENGTH="${OPTARG}"
      ;;
    s)
      USE_SPECIAL_CHARACTER='true'
      ;;
    ?)
      usage
      ;;
  esac
done

log 'Generating a password'
PASSWORD=$(date +%s%N${RANDOM}${RANDOM} | sha256sum | head -c${LENGTH})

#Append a special character
if [[ "${USE_SPECIAL_CHARACTER}" = 'true' ]]; then
  log 'Selecting a random special character.'
  SPECIAL_CHARACTER=$(echo '!@#$%^&*()-=+' | fold -w1 | shuf | head -c1)
  PASSWORD="${PASSWORD}${SPECIAL_CHARACTER}"
fi

log 'Done'

log 'Here is your password'
#display password
echo "${PASSWORD}"
exit 0
```

# Reading

[Ch 8, Software Installation and Package Repositories](https://learning.oreilly.com/library/view/linux-for-system/9781803247946/B18575_08.xhtml), Linux for System Administrators, by Viorel Rudareanu, Daniil Baturin

# Todo
- [ ] Read the Assignment 2 instructions
	- [ ] Start working on assignment 2
- [ ] Complete the reading above