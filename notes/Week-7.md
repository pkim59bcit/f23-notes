---
tags:
  - BCIT
  - Linux
topics:
  - bash
  - loops
  - conditionals
  - tests
---
# Week 7 2420  

# Learning outcomes and topics

This week:
- Write several conditional tests
- Store values in an array
- Write several for loops
- Use command substitution to store the output of a command in a variable

Next week:
- midterm exam, good luck everyone!

# printf and echo

echo and printf can both be used to write output from a script to stdout.

both echo and printf are shell builtins, they are part of the bash program.

If you need formatted output you might want to use `printf`

```bash
# try these out
printf "%20s %d\\n" ted 56
printf "%-20s %d\\n" kim 32
printf "%.3f" 1.61803398
```

## Type conversion specifier

The type conversion specifier is a character that specifies how to interpret the corresponding argument. This character is required, and it is placed after the optional fields. Below is a list showing all type conversions and what they do: 

- `%b` - Print the argument while expanding backslash escape sequences. 
- `%q` - Print the argument shell-quoted, reusable as input. 
- `%d`, `%i` - Print the argument as a signed decimal integer. 
- `%u` - Print the argument as an unsigned decimal integer. 
- `%o` - Print the argument as an unsigned octal integer. 
- `%x`, `%X` - Print the argument as an unsigned hexadecimal integer. `%x` prints lower-case letters and `%X` prints upper-case. 
- `%e`, `%E` - Print the argument as a floating-point number in exponential notation. `%e` prints lower-case letters and `%E` prints upper-case. 
- `%a`, `%A` - Print the argument as a floating-point number in hexadecimal fractional notation. `%a` prints lower-case letters and `%A` prints upper-case. 
- `%g`, `%G` - Print the argument as a floating-point number in normal or exponential notation, whichever is more appropriate for the given value and precision. `%g` prints lower-case letters and `%G` prints upper-case. 
- `%c` - Print the argument as a single character. 
- `%f` - Print the argument as a floating-point number. 
- `%s` - Print the argument as a string. 
- `%%` - Print a literal `%` symbol.

**References:** 

[Red Hat Linux command basics: printf](https://www.redhat.com/sysadmin/command-basics-printf)

# Conditionals and tests

conditionals in bash are similar to conditionals in Python and JS.

Bash includes `if` `else` and `elif` keywords. 

As well as `case`, which we will look at next week.

The general syntax of bash conditionals is:

```bash
if [[ test ]]; then
	consequent
fi
```

- [?] Why is it important that you include space between the opening and closing brackets?

`elif` and `else` example

```bash
if [[ test ]]; then
	consequent
elif [[ second test ]]; then
	consequent
else
	consequent
fi
```

Bash includes a number of conditional expressions, or tests, you can find out about them with the command `help test`.

Theses conditional expressions can be used to perform many common tests, such as checking to see if a file exists.  

## `[` vs `[[` 

Both are shell builtins, you can confirm this with the `help` command.

Both evaluate a conditional expression.

`[` is available in more shells, so if you are writing a shell script that targets the shell, not necessarily bash you may want to use this.

`[[` is a newer addition to bash and handles certain expressions differently.

One of the most significant things that you will encounter is comparison with an empty string.

- [?] Try saving both of the examples below into scripts and running them. 
	-  What happens?

```bash
#!/bin/bash

arg1=""

if [ $arg1 = "apples" ]; then
	echo "'$arg1' is 'apples' "
fi
```

```bash
#!/bin/bash

arg1=""

if [[ $arg1 = "apples" ]]; then
	echo "'$arg1' is 'apples' "
fi
```

## Exercise

complete the following work in your `~/bin` directory.

Start by creating a file named `config` that contains the following:

```
NUM1=10
NUM2=12
```

The `config` file doesn't have to be executable.

Now write a shell script in the same directory that will check to see if the config file exists and is a regular file.  This shell script needs to be executable.

If the file exists:
- import it (you can do this the same way that you re-evaluate your .bashrc )
- echo out the value of the two numbers added together
	- You can do this with arithmetic expansion `$(( expression ))`

#### Submit your shell script code [here](https://tally.so/r/mKx8QX). Just copy and paste the code into the form.

**Reference:**

[Bash manual conditional constructs](https://www.gnu.org/software/bash/manual/bash.html#Conditional-Constructs)

[Bash manual conditional expressions](https://www.gnu.org/software/bash/manual/bash.html#Bash-Conditional-Expressions)
# Working with arrays

Bash includes indexed arrays and associative arrays

Bash arrays can **not** be more than one dimensional, so you can't nest an array inside of an array.

Creating an indexed array

```bash
declare -a arr=("1" "2" "3")
# or
arr=("1" "2" "3")
# Array items are separated by whitespace
```

Accessing array values

- [?] How would you access all of the array values?

```bash
#!/usr/bin/env bash

arr=("1" "2" "3")

# access by index
echo "${arr[0]}"

# print number of array items
echo "${#arr[@]}"
```

Creating an associative array

```bash

declare -A person
person["name"]="Jan"
person["age"]="31"

```

or

- [?] In the example below what does the `r` do?

```bash
declare -rA ex_arr=(["key1"]="value1" ["key2"]="value2" ["key3"]="value3")
```

accessing values in an associative array

```bash
# print all values
echo "${ex_arr[@]}"

# print all keys
echo "${!ex_arr[@]}"

# print value by key
echo "${ex_arr["key1"]}"
```

Keys can be removed with `unset`

```bash
unset ex_arr["key1"]
```

**Reference:**

[Bash manual Arrays](https://www.gnu.org/software/bash/manual/html_node/Arrays.html)
# Loops

bash has a healthy compliment of loops, including `for`, `while` and `until`. Today we are going to focus on for loops.

for loops in bash look like for loops in most other languages

The basic syntax of a for loop is:

```bash
for item in [LIST]; do
  [COMMANDS]
done
```

You can also write c style for loops

```bash
for ((i = 0 ; i <= 1000 ; i++)); do
  echo "Counter: $i"
done
```

- [?] What will the following script do?

```bash
#!/bin/bash

for i in {01..10..2}; do
  echo $i
done
```

infinite loop in for

```bash
for ((;;)); do
	printf 'forever'
done
```

Bash includes built-ins `break` and `continue`. They behave similarly to the way they do in other languages:

**Break**

Exit from a `for`, `while`, `until`, or `select` loop. 

**Continue**

Resume the next iteration of an enclosing `for`, `while`, `until`, or `select` loop. 

## Exercise

Complete the shell script below so that a for loop will print each item in the array on a new line.

```bash
#!/usr/bin/env bash
my_items=("one" "two" "three" "four")

# your for loop here
```

#### Submit your shell script [here](https://tally.so/r/mDp0Qj)

**Reference:**

[Bash manual looping constructs](https://www.gnu.org/software/bash/manual/bash.html#Looping-Constructs)

# command substitution

Command substitution allows the output of a command to replace the command itself. Command substitution occurs when a command is enclosed as follows: 

```bash
$(command)
```

This allows you to do things like store the output of a command in a variable. For example if you wanted to store the time and date that a script was run in the variable 'now' you could use the following.

```bash
now=$(date)
```

**Reference:**

[Bash manual command substitution](https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html)

# Lab exercises

Try doing both of the exercises below on your own, don't ask your friends, or ChatGPT for the answer. 

Everything you need to do both of the labs below has either been in the reading material or course notes. 

## lab 1

Write a shell script that tests a string to see if it is a strong password

- pass your password to test to your script as a positional parameter
- test:
	- length greater than 8 characters
	- contains a lower case character
	- contains an upper case character
	- contains a number
	- contains a special character [`!@#$%^&*()_+*{}[]`]
- If one of those is missing print a weak password message warning

### Hints

You can check to see if a string contains a sub-string with 

```bash
[[ string =~ pattern ]]
```

regular expression ranges will help you with this exercise.

Here is one for special characters. 
This matches characters that are not, lower case letters, upper case letters or numbers.

```
[^a-zA-Z_0-9\s]
```

You probably want to wrap the password you are testing in quotation marks too.
## lab 2

Write a shell script that will loop over files found in a directory and write a new list of all of the markdown files that contain the word "Linux" 

- Create several files, some with the .md suffix, some with no suffix

```bash
touch file{1..4}.md file{1..4}
```

- after open several files and write "Linux" into them.
- in your script create a list of files using command substitution and the `find` command
- loop over all of the files with a for loop
- check to see if a file contains the word "Linux" with an if statement and a grep command. Also check to see if the file is a markdown file.
- If the file is a markdown file that contains the word "Linux" write the name and relative path to a new file using a redirect.

After running your script you should have a new file that contains a list of the markdown files that contain the word Linux.

When you run your script a second time you should have the same list, the files should not be added again.
### Hint

`grep -q` See the man page for grep and search for `-q`

#### Submit both labs [here](https://tally.so/r/m6KADN)
# Reading

Save the reading until after you have completed your exams.
The readings should be completed before our next regular class.
The material in the readings below will not be on the midterm.

[Using functions](https://www.linkedin.com/learning/learning-bash-scripting-17063287/using-functions)

[Selecting behavior using `case`](https://www.linkedin.com/learning/learning-bash-scripting-17063287/selecting-behavior-using-case)

[Working with options](https://www.linkedin.com/learning/learning-bash-scripting-17063287/working-with-options)

[Getting input during execution](https://www.linkedin.com/learning/learning-bash-scripting-17063287/getting-input-during-execution)

# Todo

- [ ] Study for the midterm
- [ ] Complete the reading above, after the midterm

# all of the practice questions from last week

Q. Assume that you are logged in as a regular user named "ali". Also assume that your home directory is in the standard location.
Write the full path to your home directory

A. 1 point 
`/home/ali`

Q. Explain the meaning of the following file permission notation: `-rwxr-x---`. 

A. 1 point 
User = read, write and execute, Group = read and execute, Other = nothing

Q. How could you view a list of every running process. Write a command that would do this.

A. 1 point 
`ps -ef`

Q. What is the purpose of the following directories:
- `/usr/bin`
- `/etc`

A. 2 points
- `/usr/bin` is the primary location where executable programs on the system are
- `/etc` is where default configuration files on the system are

Q. What is the purpose of the Linux `sudo` command, and how does it enhance security on a system? Provide an example of using `sudo` to execute a command with elevated privileges.

A. 2 points
The sudo command temporarily elevates a users privileges. 
This enhances security on a system by separating allowing more granular permissions on a system.  
`sudo some-command`

Q. Describe the PATH environment variable.

A. 1 point
The path environment variables holds the locations that the shell will search for a command when a command is run without an explicit path

Q. Write a command to create a new user named 'soji'. The soji user should have a home directory in the standard location with the default shell configuration files copied into it. The soji user should also use the bash shell as their login shell

A. 1 point
`useradd -ms /bin/bash soji`

Q. Write a command that would add the 'soji' user to the group 'audio'

A. 1 point
`usermod -aG audio soji`

Q. What are the three permissions that a user can have on a file?

A. 1 point
read, write and execute

Q. Write a command that would add execute permissions on a file for others.

A. 1 point
`chmod o+x file`

Q. How many users can own a file?

A. 1 point
1 user can own a file

Q. How many groups can own a file?

A. 1 point
1 group can own a file

Q. When you open vim which mode are you in?

A. 1 point
normal mode

Q. Complete the command below to save a file and quit the vim text editor.
`: `

A. 1 point
`:wq`

Q. Write a variable in bash NAME with a value of your name. Then print the value of the variable

A. 2 points
`NAME="Nathan"`
`echo $NAME`

Q. You have a file that contains the following list of file names. The files name is "files"

```
file1.txt
file2.txt
file3.txt
filea.txt
file4.txt
```

Write a grep command to that will match the first 3 files in the list above.

A. 1 point
`grep "file[123]" files`

Q. Which file has an encrypted version of user passwords? Write the full path.

A. 1 point
`/etc/shadow`

Q. Using the `grep` command, search for all occurrences of the word "Linux" within the files in the `/home/user/documents` directory and its subdirectories.

A. 1 point
`grep -r "Linux" /home/user/documents`

Q. Using the `find` command, locate all regular files with the `.txt` extension within the `/var/log` directory and its subdirectories.

A. 1 point
`find /var/log -type f -name "*.txt"`

Q. If you wanted to add a directory to your PATH for the user you are logged in as, which file would you edit?

A. 1 point
`~/.profile`

Q. In your own words describe the purpose of groups in a Linux system.

A. 2 points
Groups allow for fine grained permissions. Resources can belong to a group and users who should be able to use those resources can belong to that group

Q. When using the `pkill` utility, describe the difference between signals 15 and 9.

A. 
9 = kill and will kill a process without waiting for any child processes to shutdown
15 = terminate and will wait for child processes to shutdown before killing the main process.