---
tags:
  - BCIT
  - Linux
topics:
  - redirects
  - shell scripting
---

# Week 6 2420  

# Learning outcomes and topics

This week:
- write shell redirects to redirect file descriptors
- use a pipe to send the output from one command into another command
- write and run a shell script
- edit your shell script to use a variable
- edit your shell script to use a positional parameter
- practice mini midterm

Next week:
- More shell scripting

# File descriptors

A file descriptor is a unique, positive number used to identify an open file.

When a process makes a successful request to open a file, the kernel returns a file descriptor.

A file descriptor points to the file table, which contains information about file permissions.

You can view file descriptors with `ls` `ls -l /proc/<pid>/fd`

On a Unix-like operating system, the first three file descriptors, by default, are: stdin (standard input), stdout (standard output), and stderr (standard error).

|Name|File descriptor|Description|Abbreviation|
|---|---|---|---|
|Standard input|**0**|The default data stream for input, for example in a command pipeline. In the terminal, this defaults to keyboard input from the user.|**stdin**|
|Standard output|**1**|The default data stream for output, for example when a command prints text. In the terminal, this defaults to the user's screen.|**stdout**|
|Standard error|**2**|The default data stream for output that relates to an error occurring. In the terminal, this defaults to the user's screen.|**stderr**|

# Redirects

> Before a command is executed, its input and output may be _redirected_ using a special notation interpreted by the shell. _Redirection_ allows commands’ file handles to be duplicated, opened, closed, made to refer to different files, and can change the files the command reads from and writes to. Redirection may also be used to modify file handles in the current shell execution environment. The following redirection operators may precede or appear anywhere within a simple command or may follow a command. Redirections are processed in the order they appear, from left to right.
> - bash manual

in other words; redirects allow us to do cool things like send the output of a command to a file.

**Reference:**

[bash manual 3.6 Redirections](https://www.gnu.org/software/bash/manual/html_node/Redirections.html)
## Redirect standard out

To redirect the output from a command to a file you can use `>`

```bash
grep "alias" ~/.bashrc > aliases-bashrc

cat aliases-bashrc
```

- [?] If the file 'aliases-bashrc' doesn't exist will the command succeed?
## Redirect standard error

To redirect standard error from a command to a file you can use `2>`

```bash
find /etc -type f 2> errors

cat errors
```

A common use of this to dispose of, or not show errors. `/dev/null` is a special file that doesn't store information sent to it.

```bash
command 2> /dev/null
```

## Redirect standard out and standard error

To combine standard out and standard error you can use `&>`

## Redirect standard in

Redirecting input is a little more complex. *stdin* comes from your keyboard, you typing commands. However where we normally use *stdin* redirects is to get input from another source, like an existing file.

Assume you have a file 'names', that contains the following names:

```
kim
mo
bob
ali
ted
```

If you want to print this in alphabetical order. You could use *stdin* and the `sort` command.

```bash
sort < names
```

You could them combine this with *stdout* redirection to write the output to a file

```bash
sort < names > alphabetical-names
```

## append vs clobber

`>` will clobber, over write any existing data
`>>` will append, add the new data to the end of the file

- [?] Can you use append with *stderr*?
# pipes

You have seen pipes used a few times. Pipes are really powerful and a part of what makes working in the command line so efficient.

A common use case for this is to pipe the output of one command into another command that filters the out of the first command. This can be used to narrow down the amount of data you get so that you can better find the specific information you want.

`command1 | command2` command2's *stdin* comes from command1's *stdout*

Another use is to pipe massive amounts of data into a pager like less.

```bash
ps -ef | less
```

```bash
ls -l | grep "*.txt" | wc -l

# you can use more than one pipe in a command
docker images | cut -d' ' -f1 | tail -n +2 | sort -u | wc -l
```

# Here docs

A here document (heredoc) can be used to pipe multiple lines into a command.

basic syntax of a here doc

```bash
[COMMAND] << DELIMITER
  HERE-DOCUMENT
DELIMITER

```

An example of when you might use this is writing a script that will run commands on a server using ssh

```bash
USERNAME=ted
ssh -T user@host << _eof
  useradd -ms /bin/bash -G sudo $USERNAME
_eof
```

The `-T` option disables pseudo terminal connection, so you don't make an interactive terminal connection.
# Intro to shell scripting

## shebang

The shebang is the first line of a shell script, it tells the shell which interpreter to use when the script is executed. Another way to think of this is instead of running python scripts like `python main.py` you could just do `main.py` Instead of you specifying the interpreter in the command you are specifying the interpreter in the file.

There are two ways that we can write a shebang:

using an absolute path

```bash
#!/bin/bash
printf "hello world\n"
```

using the env utility

```python
#!/usr/bin/env python3
print("hello world")
```

- The shebang isn’t just for bash, you can use it to specify any interpreter.
- The shebang **must** be the first line in a file. In Bash and Python hashes ‘#’ represent a comment, so on subsequent lines this will be a comment, instead of a shebang.

## Hello world in bash

> The only way to learn a new programming language is by writing programs in it. The first program to write is the same for all languages: 
> _print the words `hello world`_
> - Brian Kernighan and Dennis Ritchie, _The C Programming Language_.

To get started `cd` into the "bin" directory you created last week.

Save the code below into a new file “hw” and make it executable.

Now run the file like this `hw`.

If the directory you are writing your scripts in is not in your path ($PATH), you need to prepend an absolute or relative path to your script name. 

```bash
#!/bin/bash

#: Title       : hw
#: Date        : Oct 09 2023
#: Author      : Nathan McNinch
#: Version     : 1.0
#: Description : print Hello, World!
#: Options     : None

echo "Hello, World!"
```

## single quotes vs double quotes

- [?] What is the difference between single and double quotes in bash scripts?
## echo

The builtin `echo` writes arguments to standard out.

`echo` prints a `\n` a new line after the echo line is executed.

echo can be used to print multiple lines and will preserve white space. Try adding the below to a script.

```bash
echo "
	this
		is
a multiline
echo  statement
"
```

## variables in bash

variables in bash can be declared like this:

```bash
ANIMAL=cat
```

To use a variable in your code prepend a '$'

```bash
echo "$ANIMAL"
```

Next week we will look at expansion, which is really useful when working with variables.

Using uppercase is a convention, but not required.

- [?] How could you rewrite your script to use a variable for your name? To instead of printing 'Hello, World!', you print 'Hello, your-name' 

## The `declare` builtin

In addition to the above you can use declare to provide attributes to a variable.

```bash
# declare a read only variable
declare -r CONFIG=file 

# declare an variable as an integer
declare -i NUM
```

**Reference:**

[declare, from bash manual](https://www.gnu.org/software/bash/manual/bash.html#index-declare)

## positional parameters

`$1` is a “positional parameter” These access arguments passed into the script when you run it.

If we save the code below into a new file, “print-params” make that file executable and run it like this `./print-params apple two` What is printed? What is $0?

```
#!/bin/bash

echo $0 $1 $2
```

- [?] How could you rewrite your Hello world script to use a positional parameter? So that you can pass a name to your script when you run it.

|Special Variable|Description|
|---|---|
|`$0`|The name of the bash script.|
|`$1,$2...`|The bash script arguments.|
|`$$`|The process id of the current shell.|
|`$#`|The total number of arguments passed to the script.|
|`$@`|The value of all the arguments passed to the script.|
|`$?`|The exit status of the last executed command.|
|`$!`|The process id of the last executed command.|

**Reference:**

[Bash Variables (Bash Reference Manual)](https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html)

[Bash Reference Manual](https://www.gnu.org/software/bash/manual/html_node/index.html#SEC_Contents)

[The Google Bash style guide](https://google.github.io/styleguide/shellguide.html)

[Bash scripting cheat sheet](https://devhints.io/bash)

# troubleshooting with `set`

At some point you are going write some scripts that don't work the way you want them to. A simple trick is to use `set -x` to get more detailed output. -x = xtrace.

The set builtin allows you to change the values of shell options.

```bash
#!/bin/bash

set -x

if [[ -f ~/.bashrc ]]; then
	source ~/.bashrc
fi
```

**Reference:**

[Bash manual The set Builtin](https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html)

# Why do some shell scripts have the file extension '.sh'?

This is an older convention, and has largely been dropped because .sh is ambiguous. That is .sh isn't just bash. Today most shell scripts have no file extension. 

# Practice mini midterm

![bill|400](attach/bill.jpg)

# Reading

- [Brackets and braces in Bash](https://www.linkedin.com/learning/learning-bash-scripting-17063287/brackets-and-braces-in-bash)
- [Bash expansions and substitutions](https://www.linkedin.com/learning/learning-bash-scripting-17063287/bash-expansions-and-substitutions)
- [Brace expansion](https://www.linkedin.com/learning/learning-bash-scripting-17063287/brace-expansion)
- [Parameter expansion](https://www.linkedin.com/learning/learning-bash-scripting-17063287/parameter-expansion)
- [Command substitution](https://www.linkedin.com/learning/learning-bash-scripting-17063287/command-substitution)
- [Comparing values with extended test](https://www.linkedin.com/learning/learning-bash-scripting-17063287/comparing-values-with-extended-test)
- [Formatting output with `printf`](https://www.linkedin.com/learning/learning-bash-scripting-17063287/formatting-output-with-printf)
- [Working with arrays](https://www.linkedin.com/learning/learning-bash-scripting-17063287/working-with-arrays)
- [Conditional statements with the 'if' keyword](https://www.linkedin.com/learning/learning-bash-scripting-17063287/conditional-statements-with-the-if-keyword)
- [Introducing "for" loops](https://www.linkedin.com/learning/learning-bash-scripting-17063287/introducing-for-loops)

# Todo

- [ ] Watch the videos in the reading section
- [ ] Try doing some of the quizzes in the video series for the reading