---
tags:
  - Linux
topics:
  - package-managers
---
# Week 9 2420  

## Learning outcomes and topics

- Package managers, what and why
- Update packages
- Install a package
- Search a packages
- Remove a package
- Create a deb package

## Package managers what and why

A package manager is software that manages the applications you install on your computer. Package managers can generally, install, update, remove, provide information… about software you install with them. But most importantly they handle package dependencies.

Package managers also provide a level of security. They ensure that packages are installed in a uniform format and location. In addition to security this makes it easier for a system administrator to know how the system they are administering operates. They have a better understanding of what is where.

- [?] What is the purpose of the `/opts` directory?

- [?] What is the difference between _statically_ linked libraries and _dynamically_ linked libraries?

## Package managers

Remember that different Linux distros should be treated as different operating systems. As such many distros have different package managers, and different ways of managing packages.

Debian, the distro that we have been using this term on our servers, technically, uses two different utilities to handle packages.

- **dpkg** a low level utility for working directly with package files.

- **apt** a higher level utility for managing packages installed on the system.

|   |   |   |
|---|---|---|
|Distribution|High-level package manager|Low-level package manager|
|Debian|apt|dpkg|
|Ubuntu|apt and snaps|dpkg|
|Fedora|dnf|rpm|
|RHEL|dnf|rpm|
|openSuse|zypper|zypper|
|Arch|Pacman|Pacman|
|NixOS|Nix|Nix|
|Alpine|apk|apk|

**References:**

[Chapter 8. The Debian package management tools](https://www.debian.org/doc/manuals/debian-faq/pkgtools.en.html)

## Package files

In addition to the software you are installing, a package generally includes things like help files (the man page), checksums, and information about system requirements.

> A Debian "package", or a Debian archive file, contains the executable files, libraries, and documentation associated with a particular suite of program or set of related programs. Normally, a Debian archive file has a filename that ends in `.deb`.
> 
> The internals of this Debian binary packages format are described in the deb(5) manual page. This internal format is subject to change (between major releases of Debian GNU/Linux), therefore please always use dpkg-deb(1) if you need to do lowlevel manipulations on `.deb` files.
> 
> [Debian docs](https://www.debian.org/doc/manuals/debian-faq/pkg-basics.en.html)

## Package repositories

> A Debian repository is a set of Debian binary or source packages organized in a special directory tree and with various infrastructure files - checksums, indices, signatures, descriptions translations, ... - added. Client computers can connect to the repository to download and install the packages using an [Apt](https://wiki.debian.org/Apt)-based [PackageManagement](https://wiki.debian.org/PackageManagement) tool.
> 
> [Debian docs](https://wiki.debian.org/DebianRepository)

Package repositories allow you to specify a release, or version of the Debian operating system. We are using Debian “bookworm” 12. If we wanted more up-to-date packages we could use ‘unstable’ or ‘sid’.

You could also create your own package repository and install packages from this repository with `apt`.

You can configure which repositories are used by editing the ‘debian.sources’ file in `/etc/apt/sources.list.d/` directory.

## Using `apt` to manage packages

**APT** stands for **A**dvanced **P**ackaging **T**ool.

**In class exercise one**

Using the man page for `apt` provide descriptions for all of the commands below.

When you are done submit your notes here → [https://tally.so/r/nrO7Yo](https://tally.so/r/nrO7Yo)

`sudo apt update`

`sudo apt install <package-name>`

`sudo apt upgrade`

`sudo apt autoremove <package-name>`

`apt search <package-name>`

`apt info <package-name>`

- [?] Why do you think you don’t need to use `sudo` for the last two commands?

- [?] Using the man page for apt compare `full-upgrade` and `upgrade` commands. Which do you think you would want to do as a system administrator and why?

## Creating a simple .deb package for Debian

In class exercise two, create a .deb package and install it.

To get started we need a few things. Rust tools, and the build essentials packages.

### Install build essentials

Run the command `sudo apt update`

Then run the command `sudo apt install build-essential`

### Install rust tools

The official way to install rust tools is with rustup, which we can install with shell script. Go to the link below and copy and paste the curl command into the terminal.

[https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)

### Create a hello world rust binary

To create a new Rust project run the command `cargo new hellorust`

This will create a new directory, ‘hellorust’ that contains our source code and a Cargo.toml file. cd into the ‘hellorust’ directory.

Run the command `cargo build --release`

After you should have a binary in `target/release` inside of your `hellorust` directory. Try it out by running it. `./target/release/hellorust` . You should see “Hello, world!” printed to the screen.

[https://doc.rust-lang.org/book/ch01-03-hello-cargo.html](https://doc.rust-lang.org/book/ch01-03-hello-cargo.html)

### Create a Debian package from our hellorust binary

In your home directory create a ‘Build’ directory. Inside of the build directory create a ‘hellorust’ directory and inside of this create a ‘DEBIAN’ directory. After you should have a directory structure that looks like the example below.

```
Build/
  hellorust/
    DEBIAN/
```

Inside of the ‘DEBIAN’ directory create a ‘control’ file. And copy the below into it.

```
Package: hellorust
Version: 0.1
Section: custom
Priority: optional
Architecture: amd64
Essential: no
Installed-Size: 4.5M
Maintainer: your name
Description: a hello world rust package
```

Now create a ‘usr/bin’ directory in your ‘hellorust’ directory and copy your hello world rust binary into it.

You should now have a directory structure like this

```
Build/
  hellorust/
    DEBIAN/control
    usr/bin/hellorust
```

It is finally time to build our package

run the command `dpkg-deb --build hellorust`

This will create a new .deb file ‘hellorust.deb’

After we can install it with the command `sudo dpkg -i hellorust.deb` This will install the package to `/usr/bin` so you should now be able to run your package just like any other utility.

You have just created a simple debian package. A more complex package would include things like dependencies and man pages, but this is a good start. If you are interested in creating a more complex package see the tutorials below.

**References:**

- [https://www.debian.org/doc/devel-manuals#maint-guide](https://www.debian.org/doc/devel-manuals#maint-guide)
- [https://www.debian.org/doc/devel-manuals#debmake-doc](https://www.debian.org/doc/devel-manuals#debmake-doc)

## Reading

[Arch Wiki systemd](https://wiki.archlinux.org/title/Systemd)

## Todo

- [ ] Complete course readings
- [ ] Complete assignment 2