---
tags:
  - bash
  - assignment
  - BCIT
topics:
  - bash
  - shell scripting
---

# Assignment 2: Bash scripting

## Assignment instructions

The objective of this assignment is to reinforce your knowledge of bash scripting and the use of command-line options. You will be creating several bash scripts to complete two projects: one for managing passwords and another for managing tasks.
### Due date: Sunday November 12 23:59

### Part 1: Password Manager Scripts

Write several scripts, described below, that will make up partial functionality of a password manager.

**scripts:** 

1. **Generate Passwords:** Implement a script to generate random passwords with customizable lengths. This component is partially done for you. 

2. **Store Passwords:** Users should be able to add and store passwords. You probably want to be able to use your generate password script above to create a password if one is not provided, without copying the password manually.
   A password entry should include relevant information:
	1. The password itself
	2. The service, ie google, github that the password is associated with
	3. an email address or user name associated with the service.
	4. an id

3. **List Passwords:** Create an option to list all stored passwords. Or a password for a specific service.

### Part 2: Task Manager Scripts

Write several scripts, described below, that will make up partial functionality of a task manager.

**scripts:** 

1. **Add a Task:** Implement a script to add tasks to a list, including specifying an optional due date for each task. Use the format YYYY-MM-DD for dates. A task should include:
	1. the task itself
	2. an optional due date
	3. an id

2. **List Tasks:** Create a script to list tasks. 
   Provide options to list all tasks, and tasks based on their due dates. Provide options to filter tasks by a specific due date, for example:
	- Tasks due this week.
	- Tasks due in the next three days.

3. **Delete Tasks:** Create a script to delete specific tasks. Deletion should indicate that the task is no longer on your to-do list.


**General Requirements:**

- Each script should have a user-friendly interface and display usage instructions when run with incorrect options or missing arguments. 
   
- Use getopts to handle command-line options. All of the scripts should have options.
   
- For both projects, you should provide informative help messages for users to understand how to use the script.
   
- Ensure that the scripts are well-documented with comments to explain their functionality.
  
- Store passwords and tasks in plain text files in a logical location.
	- Deciding on where to store data is part of the assignment.
	- plain text doesn't mean .txt files.

- scripts should have consistent naming
	- task_* for the task management scripts
	- pass_* for the password management scripts
	- `*` indicates the rest of the file name, ie task_add, task_delete...

This assignment will test your knowledge of bash scripting, user interaction, and command-line option handling, as well as your understanding of file systems. Good luck!

### Video demonstration

In addition to the scripts themselves you are required to create a video walk through of your scripts.

- Open the files in vim and describe the functionality of your code.
- Demonstrate how to execute your scripts with various options.
- Address any challenges or notable decisions you made during development.
### Submission:

submit your scripts and a 'README.md' file that contains a link to your video in a `your-name-assignment2.zip` 

I should be able to watch the video without creating an account or logging into a service, otherwise you can host the videos anywhere (youtube, vimeo...)

Scripts should be unix formatted, not dos. Windows adds an extra character to new lines in documents, this breaks documents for MacOS, Linux... Basically every other operating system. Test this out before submitting, because I will ask you to resubmit if scripts are dos formatted.

## Assignment grading: Total 24 points

**Part 1: Password Manager Scripts (6 Points)**

1. **Store Passwords (2 points):**
   - [ ] Users can add and store passwords securely.
   - [ ] Appropriate information (password, service, email/username) is included.

2. **List Passwords (2 points):**
   - [ ] Option to list all stored passwords is implemented.
   - [ ] Option to list passwords for a specific service is implemented.

3. **Code Quality (2 points):**
   - [ ] Code is well-documented with comments.
   - [ ] Code is properly formatted.
   - [ ] Usage instructions are displayed when incorrect options or missing arguments are provided.

**Part 2: Task Manager Scripts (8 Points)**

1. **Add a Task (2 points):**
   - [ ] Option to add tasks to a list is implemented.
   - [ ] Due date can be specified for each task.

2. **List Tasks (2 points):**
   - [ ] Option to list all tasks is implemented.
   - [ ] Option to list tasks based on due dates is implemented.

3. **Delete Tasks (2 points):**
   - [ ] Option to delete specific tasks is implemented.
   - [ ] Deletion indicates that the task is no longer on the to-do list.

4. **Code Quality (2 points):**
   - [ ] Code is well-documented with comments.
   - [ ] Code is properly formatted.
   - [ ] Usage instructions are displayed when incorrect options or missing arguments are provided.

**Video Demonstration (8 Points):**

**Video Content (8 points):**
   - [ ] Video walk-through is clear, concise and complete (2 points).
   - [ ] Key functionality of the scripts is explained and demonstrates understanding of script functionality (5 points).
   - [ ] Script execution is demonstrated with various options (1 point).

**General Requirements (2 Points):**

**Submission (2 points):**
- [ ] Scripts, 'README.md' file with a video link, and other requirements are correctly submitted in the specified format using specified naming conventions. (2 point).

**Total Points (out of 24):**

**Late marks**
10% will be deducted per day up to 4 days. 
After 4 days the assignment will not be graded. Reasonable exceptions will of course be made.
## Additional information

It might be helpful to generate random ids for the scripts above. Instead of trying to use 1234... Here is an example of how you could do that.

```bash
#!/bin/bash

# Generate a unique random ID
unique_id=$(($(date +%s%N) + $RANDOM))

echo "Unique ID: $unique_id"

```

To transfer your scripts from your debian environments (DO droplets) use `sftp`. 