# Assignment 1: Independent Research Video

## Assignment instructions

 **Group Formation**    
- Form groups of two.
- Each group selects **3** topics that will become proposals 
- Topics must be related to Linux, system administration.
- Topics cannot be topics covered in class, or the networking class.
- Topics cannot be a distro review.

**Proposal Submission**  
- Submit three topic ideas for approval.
- I will assist in refining one of the proposed ideas.
- Approved topics are finalized and you can begin the research.

**Research Phase**  
- In your group conduct research on your chosen Linux topic.
- Research can include historical background, technical details, case studies, and more.
- Proper citations and references are essential. Keep references to the material that you find. Try not to just regurgitate an existing youtube tutorial on a topic.

**Video Presentation**     
- Create a 10-minute video presentation on your research.
- Components of the video presentation:

- Introduction to the topic.  
- Historical context (if applicable).
- Technical aspects and features.
- Real-world applications.
- Challenges and solutions.
- Conclusion and future prospects.

- Visual aids, diagrams, and demonstrations are encouraged.

**Submission**     
- Groups submit their video presentations along with a written summary of their research.
- Your research summary is just a list of resources that you used to learn about your topic. With a brief summary of the source. This will be a .pdf file.
- Your video should be hosted somewhere so that I can watch it, without downloading it.

Submit using the D2L dropbox
Submit everything in the same dropbox, proposal, link to video, research summary
You can submit multiple documents.
Everything must be a .pdf

==Please include all team members names on every document that you submit==

## Assignment grading

**Proposal Component (10 points)**     
- **Three Proposals:**
- Each proposal submitted: 2 points (6 points total)
- Relevance and creativity of proposals: 4 points

**Video Presentation Component (40 points)**     
- **Content (20 points):**
- Introduction to the topic: 3 points
- Historical context (if applicable): 3 points
- Technical aspects and features: 5 points
- Real-world applications: 4 points
- Challenges and solutions: 3 points
- Conclusion and future prospects: 2 points

- **Clarity and Organization (10 points):**
- Logical flow and structure of the presentation: 4 points
- Visual aids and diagrams (if used): 3 points
- Overall clarity of the presentation: 3 points

- **Quality of Research (10 points):**
- Depth and relevance of research materials: 4 points
- Proper citations and references: 3 points
- Demonstrated understanding of the topic: 3 points

**Teamwork Component (10 points)**      
- **Collaboration (5 points):**
- Evidence of effective collaboration between group members: 3 points
- Equal contribution from both group members: 2 points

- **Overall Understanding (5 points)**     
- Demonstrated Knowledge (3 points)
- Engagement with topic (2 points)

**Total Points: 60**  

_Grading Scale:_

- 54-60 points: Excellent
- 44-53 points: Good
- 30-43 points: Satisfactory
- Below 30 points: Needs Improvement

## Additional notes

**Citations and References:**

- Use a consistent citation style (e.g., APA, MLA, Chicago).
- Properly cite all sources of information, including websites, books, articles, and videos.